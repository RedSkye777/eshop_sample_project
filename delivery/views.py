import logging
import json

from cdek.api import CDEKClient

from django.http import JsonResponse
from django.conf import settings
from django.http import HttpResponseBadRequest
from django.db.models import ObjectDoesNotExist

from delivery.models import City
from main.utils import ajax_required
from cart.cart import Cart

logger = logging.getLogger(__name__)
logger.setLevel('DEBUG')

formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler('logs/delivery.log', encoding="UTF-8")
file_handler.setLevel('DEBUG')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

if settings.DEBUG:
    cdek_client = CDEKClient(settings.CDEK_ACCOUNT_NAME, settings.CDEK_ACCOUNT_PASSWORD, test=True)
else:
    cdek_client = CDEKClient(settings.CDEK_ACCOUNT_NAME, settings.CDEK_ACCOUNT_PASSWORD)

@ajax_required
def get_city_prompt(request):
    response_data = {}

    if request.method == 'GET':

        query = request.GET.get('query')
        suggestions = []

        logger.debug(query)

        if query:

            cities = City.objects.filter(full_name__istartswith=query).distinct()[:7]

            for city in cities:
                suggestions.append({
                    'value': city.full_name,
                    'city_id': city.city_id
                })

        response_data['query'] = query
        response_data['suggestions'] = suggestions
            
    return JsonResponse(response_data)

@ajax_required
def get_cdek_delivery_points(request):
    response_data = {'pvz_list': []}

    if request.method == 'GET':
        cdek_city_id = request.GET.get('cdek_city_id')

        if cdek_city_id:
            cdek_request = cdek_client.get_delivery_points(city_id=cdek_city_id)

            response_data['pvz_list'] = cdek_request['pvz']
    
    return JsonResponse(response_data)

@ajax_required
def get_cdek_shipping_cost(request):

    if not request.method == 'GET':
        return HttpResponseBadRequest()
    
    receiver_city_id = request.GET.get('receiver_city_id')
    delivery_point_id = request.GET.get('delivery_point_id')
    sender_city_id = request.GET.get('sender_city_id')
    tariff_id = request.GET.get('tariff_id')

    if not sender_city_id:
        sender_city_id = 44
    
    if not receiver_city_id:
        return JsonResponse({'errors': 'receiver_city_id is not found'})
    
    if not tariff_id:
        return JsonResponse({'errors': 'tariff_id is not found'})
    
    cart = Cart(request)
    product_dimension_list =[]

    for item in cart:
        product = item['product']
        product_dimension_data = {}

        product_dimension_data['weight'] = product.weight
        product_dimension_data['length'] = product.length
        product_dimension_data['width'] = product.width
        product_dimension_data['height'] = product.height

        if product.volume:
            product_dimension_data['volume'] = product.volume

        product_dimension_list.append(
            product_dimension_data
        )

    shipping_costs = cdek_client.get_shipping_cost(
        sender_city_id=sender_city_id,
        receiver_city_id=receiver_city_id,
        goods=product_dimension_list,
        tariff_id=tariff_id,
    )

    try:
        return JsonResponse({'error': shipping_costs['error'], 'status': 'ko'})
    except KeyError:
        return JsonResponse({'shipping_cost': shipping_costs, 'status': 'ok'})
from django.urls import path

from delivery import views

app_name = 'delivery'

urlpatterns = [
    path(
        'get-city-prompt/',
        views.get_city_prompt,
        name='get_city_prompt'
    ),
    path(
        'get-cdek-shipping-cost/',
        views.get_cdek_shipping_cost,
        name='get_cdek_shipping_cost'
    ),
    path(
        'get-cdek-delivery-points/',
        views.get_cdek_delivery_points,
        name='get_cdek_delivery_points'
    ),
]

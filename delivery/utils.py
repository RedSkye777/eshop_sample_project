import csv

from django.db import models
from django.conf import settings

from delivery.models import City

def update_delivery_cities():

    if settings.DEBUG:
        max_rows = 50000
    else:
        max_rows = 5000

    document = open('delivery/city_base/cities_rus.csv', encoding="utf8")
    reader = csv.reader(document)

    counter = 1

    for row in reader:
        if counter != 1:
            try:
                city = City.objects.get(city_id=row[0])
                city.name = row[2]
                city.full_name = row[1]
                city.region_name = row[3]
                city.postcode_list=row[7]
            except models.ObjectDoesNotExist:
                City.objects.create(
                    city_id=row[0],
                    name=row[2],
                    full_name=row[1],
                    region_name=row[3],
                    postcode_list=row[7]
                )
        counter += 1

        if counter >= max_rows:
            break

    document.close()
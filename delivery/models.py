from django.db import models

# "ID","FullName","CityName","OblName","Center",
# "NalSumLimit","EngName","PostCodeList","EngFullName","EngOblName",
# "CountryCode","CountryName","EngCountryName","FullNameFIAS",
# "FIAS","KLADR","cityDD","pvzCode"
class City(models.Model):
    city_id = models.PositiveIntegerField()
    name = models.TextField()
    full_name = models.TextField()
    region_name = models.TextField()
    postcode_list = models.TextField(null=True)

    def __str__(self):
        return self.full_name

    
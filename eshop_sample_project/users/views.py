import logging

from django.shortcuts import redirect, render
from django.db.models import F, FloatField, Sum, Prefetch, ObjectDoesNotExist
from django.urls import reverse_lazy
from django.views.generic import FormView
from django.views.generic.base import TemplateView
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.http import JsonResponse, HttpResponseBadRequest

from . import forms
from main.utils import check_recaptcha, ajax_required, get_form_errors, query_debugger
from main.models import Product
from order.models import Order, OrderItem

logger = logging.getLogger(__name__)
logger.setLevel('INFO')

formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler('logs/account_info.log')
file_handler.setLevel('INFO')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

@check_recaptcha
def signup_view(request):
    successful_url = request.GET.get('next', '/')
    context_data = {
        'recaptcha_public_key': settings.RECAPTCHA_PUBLIC_KEY,
    }

    if request.method == 'POST':
        form = forms.UserCreationForm(data=request.POST, request=request)
        
        if form.is_valid():
            form.save()
            form.signup_success()
            return redirect(successful_url)
        else:
            context_data['form'] = form

    return render(request, 'account/signup.html', context_data)


@check_recaptcha
def login_view(request):
    successful_url = request.GET.get('next', '/')
    context_data = {
        'recaptcha_public_key': settings.RECAPTCHA_PUBLIC_KEY,
    }

    if request.method == 'POST':
        form = forms.AuthenticationForm(data=request.POST, request=request)

        if form.is_valid():

            try:
                cart = request.session[settings.CART_SESSION_ID]
            except KeyError:
                cart = None
            
            try:
                order = request.session[settings.ORDER_SESSION_ID]
            except KeyError:
                order = None

            form.authentication_success()

            try:
                request.session[settings.CART_SESSION_ID]
            except KeyError:
                if cart:
                    request.session[settings.CART_SESSION_ID] = cart
            
            try:
                request.session[settings.ORDER_SESSION_ID]
            except KeyError:
                if order:
                    request.session[settings.ORDER_SESSION_ID] = order
                
            return redirect(successful_url)
        else:
            context_data['form'] = form

    return render(request, 'account/login.html', context_data)

def user_logout(request):
    successful_url = request.GET.get('next', '/')

    try:
        cart = request.session[settings.CART_SESSION_ID]
    except KeyError:
        cart = None
    
    try:
        order = request.session[settings.ORDER_SESSION_ID]
    except KeyError:
        order = None
    
    logout(request)

    if cart:
        request.session[settings.CART_SESSION_ID] = cart
    
    if order:
        request.session[settings.ORDER_SESSION_ID] = order
    
    return redirect(successful_url)

class PasswordResetView(auth_views.PasswordResetView):
    template_name = 'account/password_reset_form.html'
    email_template_name = 'account/password_reset_email.html'
    success_url = reverse_lazy('account:password_reset_done')
    from_email = settings.PROJECT_EMAIL_ADDRESS

class PasswordResetDoneView(auth_views.PasswordResetDoneView):
    template_name = 'account/password_reset_done.html'

class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    template_name = 'account/password_reset_confirm.html'
    success_url = reverse_lazy('account:password_reset_complete')

class PasswordResetCompleteView(auth_views.PasswordResetCompleteView):
    template_name = 'account/password_reset_complete.html'

class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'account/profile.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user = self.request.user
        context['first_name'] = user.first_name
        context['last_name'] = user.last_name
        context['phone_number'] = user.phone_number
        context['email'] = user.email
            
        return context

@login_required
def purchase_history(request):
    user = request.user

    orderItem_querySet = OrderItem.objects                                                             \
        .select_related('product')                                                                     \
        .only('price', 'quantity', 'order_id', 'product_id', 'product__image', 'product__name',)       \
        .order_by('-quantity')
        
    order_list = Order.objects.filter(user=user)                                                       \
        .annotate(total_cost=Sum(F('items__quantity') * F('items__price'), output_field=FloatField())) \
        .only('payment_status', 'created')                                                             \
        .prefetch_related(Prefetch('items', queryset=orderItem_querySet))                              \
        .order_by('-created')

    return render(request, 'account/profile_orders_history.html', {'order_list': order_list})
        
    
@login_required
def favorites(request):

    if request.method == 'GET':
        user = request.user
        queryset = user.favourites.only('price', 'name', 'image')

        return render(request, 'account/profile_favorite.html', {'favourite_list': queryset})
    
    elif request.method == 'POST':
        action = request.POST.get('action')
        product_id = request.POST.get('product_id')

        error_list = []
        form = forms.ValidateFavouriteProducts(request=request, data=request.POST)

        if form.is_valid():
            if action == 'add-product':
                form.add_product() 
            elif action == 'remove-product':
                form.remove_product()
        else:
            error_list = get_form_errors(form)
        
        return JsonResponse({'errors': error_list})

    else:
        return HttpResponseBadRequest()


@ajax_required
def update_user_data(request):
    error_list = []

    if request.method == 'POST':
        form = forms.UpdateUserData(data=request.POST, request=request)

        if form.is_valid():
            form.update_data()

            return JsonResponse({'status': 'ok'})
        else:
            error_list = get_form_errors(form=form)
    
    return JsonResponse({'status': 'ko', 'errors': error_list})
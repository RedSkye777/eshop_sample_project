import logging

from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.db.models import ObjectDoesNotExist
from django.core.mail import send_mail

from . import models

logger = logging.getLogger(__name__)
logger.setLevel('INFO')

class UserCreationForm(forms.Form):
    '''
    A form that creates a user, with no privileges, from the given username and
    password.
    '''
    error_messages = {
        'email_missed': 'Вы не ввели email адрес.',
        'password_mismatch': 'Пароли не совпадают.',
        'password1_missed': 'Вы не ввели пароль.',
        'password2_missed': 'Вы не ввели пароль повторно.',
        'policy_agreement_declined': 'Вы не согласны с условиями пользовательского соглашения.',
        'recaptcha_missed': 'Поставьте галочку, если Вы не робот.',
        'user_already_exists': 'Пользователь с данным email уже существует.'
    }
    email = forms.EmailField(required=False)
    password1 = forms.CharField(required=False)
    password2 = forms.CharField(required=False)
    policy_agreement = forms.CharField(required=False)

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    def clean(self):
        email = self.cleaned_data.get('email')
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        policy_agreement = self.cleaned_data.get('policy_agreement')

        if not email:
            raise forms.ValidationError(
                self.error_messages['email_missed'],
                code='email_missed',
            )
        if not password1:
            raise forms.ValidationError(
                self.error_messages['password1_missed'],
                code='password1_missed',
            )
        if not password2:
            raise forms.ValidationError(
                self.error_messages['password2_missed'],
                code='password2_missed',
            )
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        if not policy_agreement:
            raise forms.ValidationError(
                self.error_messages['policy_agreement_declined'],
                code='policy_agreement_declined',
            )
        if not settings.TESTING_MODE:
            if not self.request.recaptcha_is_valid:
                raise forms.ValidationError(
                    self.error_messages['recaptcha_missed'],
                    code='recaptcha_missed'
                )
        if email:
            if models.User.objects.filter(email=email).exists():
                raise forms.ValidationError(
                    self.error_messages['user_already_exists'],
                    code='user_already_exists',
                )

    def save(self, commit=True):
        user = models.User.objects.create_user(
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password1']
        )
        return user
    

    def send_greetings_mail(self):
        logger.info(
            'Sending signup email for email=%s',
            self.cleaned_data['email'],
        )
        message = 'Добро пожаловать {}'.format(self.cleaned_data['email'])
        send_mail(
            'Поздравляем с регистрацией на YourShopName.ru',
            message,
            settings.PROJECT_EMAIL_ADDRESS,
            [self.cleaned_data['email']],
            fail_silently=True,
        )
    
    def signup_success(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password1')
        user = authenticate(email=email, password=password)
        
        login(self.request, user)
        self.send_greetings_mail()

class AuthenticationForm(forms.Form):
    '''
    A form that authenticates user with the given email and
    password.
    '''
    error_messages = {
        'email_missed': 'Вы не ввели email адрес.',
        'password_missed': 'Вы не ввели пароль.',
        'recaptcha_invalid': 'Поставьте галочку, что вы не робот.',
        'access_denied': 'Введёные данные неверны. Попробуйте снова. '
    }

    email = forms.EmailField(required=False)
    password = forms.CharField(required=False)

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user = None
        super().__init__(*args, **kwargs)
    
    def clean(self):
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")

        if not email:
            raise forms.ValidationError(
                    self.error_messages['email_missed'],
                    code='email_missed'
                )
        if not password:
            raise forms.ValidationError(
                    self.error_messages['password_missed'],
                    code='password_missed'
                )
        if not settings.TESTING_MODE:
            if not self.request.recaptcha_is_valid:
                raise forms.ValidationError(
                        self.error_messages['recaptcha_invalid'],
                        code='recaptcha_invalid'
                    )
        self.user = authenticate(
            self.request, email=email, password=password
        )

        if self.user is None:
            raise forms.ValidationError(
                    self.error_messages['access_denied'],
                    code='access_denied'
                )

    
    def authentication_success(self):
        # Login user after authentication

        login(self.request, self.user)

class UpdateUserData(forms.Form):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    phone_number = forms.CharField(required=False)

    error_messages = {
        'user_is_not_found': 'User is not found or is anonimous.',
    }

    def __init__(self, request=None, *args, **kwargs):
        self.user = request.user
        super().__init__(*args, **kwargs)

    def clean(self):
        if not self.user.is_authenticated:
            raise forms.ValidationError(
                self.error_messages['user_is_not_found'],
                code='user_is_not_found'
            )

    def update_data(self):
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        phone_number = self.cleaned_data.get('phone_number')

        user = self.user

        if first_name:
            user.first_name = first_name
        
        if last_name:
            user.last_name = last_name
        
        if phone_number:
            user.phone_number = phone_number
        
        user.save()

class ValidateFavouriteProducts(forms.Form):
    action = forms.CharField()
    product_id = forms.UUIDField()

    action_list = [
        'add-product',
        'remove-product'
    ]

    error_messages = {
        'user_is_anonymous': 'Anonymous user can not add product to favourite list.',
        'action_is_not_found': 'Action is not found.',
        'product_id_is_not_found': 'product_id is not found.',
        'action_is_not_available': 'Action is not available.',
        'product_id_is_not_correct': 'product_id is not correct.'
    }


    def __init__(self, request=None, *args, **kwargs):
        self.user = request.user
        super().__init__(*args, **kwargs)

    def clean(self):
        action = self.cleaned_data.get('action')
        product_id = self.cleaned_data.get('product_id')

        if not self.user.is_authenticated:
            raise forms.ValidationError(
                self.error_messages['user_is_anonymous'],
                code='user_is_anonymous'
            )
        if not action:
            raise forms.ValidationError(
                self.error_messages['action_is_not_found'],
                code='action_is_not_found'
            )
        if not product_id:
            raise forms.ValidationError(
                self.error_messages['product_id_is_not_found'],
                code='product_id_is_not_found'
            )
        if action not in self.action_list:
            raise forms.ValidationError(
                self.error_messages['action_is_not_available'],
                code='action_is_not_available'
            )
        try:
            product = Product.objects.get(id=product_id)
        except ObjectDoesNotExist:
            raise forms.ValidationError(
                self.error_messages['product_id_is_not_correct'],
                code='product_id_is_not_correct'
            )

    def add_product(self):
        user = self.user
        product_id = self.cleaned_data.get('product_id')
        
        product = Product.objects.get(id=product_id)

        user.favourites.add(product)
    
    def remove_product(self):
        user = self.user
        product_id = self.cleaned_data.get('product_id')
        
        product = Product.objects.get(id=product_id)

        user.favourites.remove(product)

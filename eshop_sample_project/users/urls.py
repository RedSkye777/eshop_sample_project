from django.urls import path

from . import views

app_name = 'account'

urlpatterns = [
    path(
        'update-user-data/',
        views.update_user_data,
        name='update_user_data'
    ),
    path(
        'favorites/',
        views.favorites,
        name='favorites'
    ),
    path(
        'purchase_history/',
        views.purchase_history,
        name='purchase_history'
    ),
    path(
        'signup/',
        views.signup_view,
        name='signup'
    ),
    path(
        'login/',
        views.login_view,
        name='login'
    ),
    path(
        'logout/',
        views.user_logout,
        name='logout'
    ),
    path(
        'password_reset/',
        views.PasswordResetView.as_view(),
        name='password_reset'
    ),
    path(
        'password_reset/done/',
        views.PasswordResetDoneView.as_view(),
        name='password_reset_done'
    ),
    path(
        'reset/<uidb64>/<token>/',
        views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'
    ),
    path(
        'reset/done/',
        views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete'
    ),
    path(
        'profile/',
        views.ProfileView.as_view(),
        name='profile'
    ),
]
// начало выпадающего меню слева
$(document).ready(function() {
    let Accordion = function(el, multiple) {
        this.el = el || {};
        // more then one submenu open?
        this.multiple = multiple || false;

        let dropdownlink = this.el.find('.dropdownlink-category');
        dropdownlink.on('click', {
                el: this.el,
                multiple: this.multiple
            },
            this.dropdown);
    };

    Accordion.prototype.dropdown = function(e) {
        let $el = e.data.el,
            $this = $(this),
            //this is the ul.submenuItems
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');
    }

    let accordion = new Accordion($('.accordion-menu-category'), false);
});

// проверка на отсутствие букаф и знакаф для диапазона цен
$(document).ready(function() {
    $('#price-input-from').on('input', function() {
        $(this).val($(this).val().replace(/[^\d]/g, ''))
    });

    $('#price-input-to').on('input', function() {
        $(this).val($(this).val().replace(/[^\d]/g, ''))
    });

    $('#price-input-from-small').on('input', function() {
        $(this).val($(this).val().replace(/[^\d]/g, ''))
    });

    $('#price-input-to-small').on('input', function() {
        $(this).val($(this).val().replace(/[^\d]/g, ''))
    });

})

// открытие бокового меню категорий
$('.slide-block-categorys-btn').click(function() {
    document.getElementById("slide-block-categorys").style.display = "block";
    $('#layer').fadeIn('fast');
    $('.navbar-main-small').css('z-index', 1999);
});
// закрытие бокового меню категорий
function closeCategory() {
    document.getElementById("slide-block-categorys").style.display = "none";
    $('#layer').fadeOut('fast');
    $('.navbar-main-small').css('z-index', 2000);
}

// открытие бокового меню фильтров
$('.slide-block-filters-btn').click(function() {
    document.getElementById("slide-block-filters").style.display = "block";
    $('#layer').fadeIn('fast');
    $('.navbar-main-small').css('z-index', 1999);
});
// закрытие бокового меню фильтров
function closeFilters() {
    document.getElementById("slide-block-filters").style.display = "none";
    $('#layer').fadeOut('fast');
    $('.navbar-main-small').css('z-index', 2000);
}


// значения текущего диапазона цен
$(document).ready(function() {
    $('.range-input-first').change(function() {
        let curMin = $(this).val();
        $('.current-min-price').text(curMin);
    })

    $('.range-input-last').change(function() {
        let curMax = $(this).val();
        $('.current-max-price').text(curMax);
    })
})


$(document).ready(function() {

    // Действие при нажатии на кнопку добавления товара в корзину
    $(document).on('click', '.btn-add-to-basket', function(e) {
        e.preventDefault();
        let product_id = $(this).data("id");
        let action = 'add-to-cart'

        let send_data = {
            quantity: "1",
            product_id: product_id
        }

        sendProductData(send_data, $(this), action)
    });

    // Действие при нажатии на кнопку добавления товара в избранное
    $(document).on('click', '.btn-outline-secondary', function(e) {
        e.preventDefault();
        let product_id = $(this).data("id");
        let action = 'add-to-favourites'

        let send_data = {
            product_id: product_id,
            action: 'add-product'
        }

        sendProductData(send_data, $(this), action)
    });
})

// Функция отправляет данные на сервер, и в случае успешного ответа происходит вызов функции "redrawPageInfo"
function sendProductData(send_data, element, action) {
    let url = ''

    if (action == 'add-to-cart') {
        url = '/cart/add/'
    } else if (action == 'add-to-favourites') {
        url = '/account/favorites/'
    } else {
        console.log('Action is not available.');
        return false;
    }

    $.ajax({
        method: 'POST',
        url: url,
        data: send_data,
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function(result) {
            redrawPageInfo(result, element, action)
        },
        error: function(response) {
        }
    });
}

// Function that redraws page info
function redrawPageInfo(data, element, action) {
    if (action == 'add-to-cart') {
        if (data.status == 'ok') {
            let product_id = data.product_id
            let product_price_html = $(element).data('product-price')
            let add_to_cart_button = $(element)
            let basket_miniature_html = $('#basket-form')
            let basket_detail_url = '/cart/'
            let cart_length_html = $('#basket-count')
            let cart_total_cost_html = $(basket_miniature_html).find('#minituare-basket-total-cost')

            let product_price_int = parseInt(product_price_html)
            let cart_length_int = parseInt($(cart_length_html).text())
            let cart_total_cost_int = parseInt($(cart_total_cost_html).text())

            let updated_cart_length_int = 1
            let updated_cart_total_cost_int = product_price_int

            if (!isNaN(cart_length_int)) {
                updated_cart_length_int = (cart_length_int + 1)
            }

            if (!isNaN(cart_total_cost_int)) {
                updated_cart_total_cost_int = (cart_total_cost_int + product_price_int)
            }

            // Изменяем внешний вид и свойства миниатюры-корзины
            basket_miniature_html.children().remove()
            basket_miniature_html.append(
                '<a href="' + basket_detail_url + '">' +
                '<div id="basket-area" class="btn btn-danger">' +
                '<div class="cart-area">' +
                '<ion-icon class="icon-cart-large" name="cart"></ion-icon>' +
                '<span class="span-for-cart-quantity"><span id="basket-count">' + updated_cart_length_int + '</span></span>' +
                '</div>' +

                '<div class="count-area">' +
                '<span id="minituare-basket-total-cost">' + updated_cart_total_cost_int + ' р.' + '</span>' +
                '</div>' +
                '</div>' +
                '</a>'
            )

            // Добавляем класс кнопке add-to-cart
            $(add_to_cart_button).addClass("active-add-to-basket");
        }
    }

    if (action == 'add-to-favourites') {
        if (data.errors.length < 1) {
            $(element).addClass("active-instant-buy")
        } else {
            console.log(result.errors);
        }
    }
}
$(document).ready(function () {
    var picture_miniatures = $('.wrapper-for-small-pictures').children()
    var main_product_picture = $('#product-picture-url')

    picture_miniatures.mouseenter(function () {
        var miniature_picture_url = $(this).attr('src')
        main_product_picture.attr('src', miniature_picture_url)
    })
})

// Отправка информации о заказе на сервер при добавлении товара в корзину
$(document).ready(function () {

    let nameid = document.getElementById('caption-product');
    id = nameid.dataset.id;

    let data_for_sending = {
        quantity: "1",
        product_id: id
    }

    $('.btn-add-to-basket').click(function (e) {
        e.preventDefault();

        // отправка данных при нажатии на кнопку
        sendProductData(data_for_sending)
    });
})

// Function that sends product data to the server
function sendProductData(data_for_sending) {
    let url = '/cart/add/'

    $.ajax({
        method: 'POST',
        url: url,
        data: data_for_sending,
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function (result) {
            if (result.status == 'ok') {
                redrawPageInfo()
            }
        },
        error: function (response) {
        }
    });
}

// Function that redraws page info
function redrawPageInfo() {
    let product_price_html = $('.price-of-the-product-now')
    let basket_miniature_html = $('#basket-form')
    let basket_detail_url = '/cart/'
    let cart_length_html = $('#basket-count')
    let cart_total_cost_html = $(basket_miniature_html).find('#minituare-basket-total-cost')

    let product_price_int = parseInt($(product_price_html).text())
    let cart_length_int = parseInt($('#basket-count').text())
    let cart_total_cost_int = parseInt($(basket_miniature_html).find('#minituare-basket-total-cost').text())

    let updated_cart_length_int = 1
    let updated_cart_total_cost_int = product_price_int

    if (!isNaN(cart_length_int)) {
        updated_cart_length_int = (cart_length_int + 1)
    }

    if (!isNaN(cart_total_cost_int)) {
        updated_cart_total_cost_int = (cart_total_cost_int + product_price_int)
    }

    basket_miniature_html.children().remove()
    basket_miniature_html.append(
        '<a href="' + basket_detail_url + '">' +
        '<div id="basket-area" class="btn btn-danger">' +
        '<div class="cart-area">' +
        '<ion-icon class="icon-cart-large" name="cart"></ion-icon>' +
        '<span class="span-for-cart-quantity"><span id="basket-count">' + updated_cart_length_int + '</span></span>' +
        '</div>' +

        '<div class="count-area">' +
        '<span id="minituare-basket-total-cost">' + updated_cart_total_cost_int + ' ₽' + '</span>' +
        '</div>' +
        '</div>' +
        '</a>'
    )
}
// подготовка ajax-post запросов
var csrftoken = Cookies.get('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});



// прелоадер
// $(window).on('load', function () {
//     let $preloader = $('#page-preloader'),
//         $spinner   = $preloader.find('.spinner');
//     $spinner.fadeOut();
//     $preloader.delay(350).fadeOut('slow');
// });

// активация подсказок
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="tooltip"]').on('click', function() {
        $(this).tooltip('hide');
    });
});

//   $(function () {
//     $('[data-toggle="tooltip"]').tooltip({ trigger : 'hover'})
// })

// скрыть\показать меню при скролле
let header = $('.hide-nav'),
    scrollPrev = 0;

$(window).scroll(function() {
    let scrolled = $(window).scrollTop();

    if (scrolled > 100 && scrolled > scrollPrev) {
        header.addClass('out');
    } else {
        header.removeClass('out');
    }
    scrollPrev = scrolled;
});

// активация выпадающих меню
$(document).ready(function() {
    $(".dropdown-toggle-js").dropdown();
});

// ветки каталога товаров и их выпадение на больших экранах
$(document).ready(function() {
    let Accordion = function(el, multiple) {
        this.el = el || {};
        // more then one submenu open?
        this.multiple = multiple || false;

        let dropdownlink = this.el.find('.dropdownlink');
        dropdownlink.on('click', {
                el: this.el,
                multiple: this.multiple
            },
            this.dropdown);
    };

    Accordion.prototype.dropdown = function(e) {
        let $el = e.data.el,
            $this = $(this),
            //this is the ul.submenuItems
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            //show only one menu at the same time
            $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
        }
    }

    let accordion = new Accordion($('.accordion-menu'), false);
})

// ветки каталога товаров и их выпадение на малых экранах
$(document).ready(function() {
    let Accordion = function(el, multiple) {
        this.el = el || {};
        // more then one submenu open?
        this.multiple = multiple || false;

        let dropdownlink = this.el.find('.dropdownlink');
        dropdownlink.on('click', {
                el: this.el,
                multiple: this.multiple
            },
            this.dropdown);
    };

    Accordion.prototype.dropdown = function(e) {
        let $el = e.data.el,
            $this = $(this),
            //this is the ul.submenuItems
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            //show only one menu at the same time
            $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
        }
    }

    let accordion = new Accordion($('.accordion-menu-small'), false);
})

// для предотвращения случайного закрытия каталога
$(document).on("click.bs.dropdown.data-api", ".noclose", function(e) {
    e.stopPropagation()
});

// открытие бокового меню
$('#navbar-small-btn-open').click(function() {
    document.getElementById("mySidenav").style.display = "block";
    $('#layer').fadeIn('fast');
    $('.navbar-main-small').css('z-index', 1999);
});

// закрытие бокового меню
function closeNav() {
    document.getElementById("mySidenav").style.display = "none";
    $('#layer').fadeOut('fast');
    $('.navbar-main-small').css('z-index', 2000);
}

(function() {
    $(document).on('click', '.search-button', function() {
        $(this).parent().parent().toggleClass('active');
    });
});


// показать поиск на маленьком меню
function viewSearch() {
    document.getElementById("search-wrap").style.display = "block";
    $('.search-wrap').css({
        'position': 'absolute',
        'z-index': '1000'
    });
};


// скрыть поиск на маленьком меню
function hideSearch() {
    document.getElementById("search-wrap").style.display = "none";
    $('#search-wrap').css({
        'position': 'absolute',
        'z-index': '0'
    });
};


// Поиск на большом и маленьнком экранах
$(document).ready(function() {
    $('#search-input').bind("change paste keyup", function() {
        let query = $(this).val()
        let screen_size = 'dekstop'

        sendSearchQuery(query, screen_size)
    });

    $('.search-input-small').bind("change paste keyup", function() {
        let query = $(this).val()
        let screen_size = 'tablet'

        sendSearchQuery(query, screen_size)
    });
})

// закрытие выпадающего списка поиска при нажатии вне его
$(document).mouseup(function(e) {
    let div = $(".dropdown-results");
    if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        div.hide();
    }
});

$(document).mouseup(function(e) {
    let div = $(".dropdown-results-small");
    if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        div.hide();
    }
});


/*Dropdown Menu*/
$(document).ready(function() {
    $('.dropdown').click(function() {
        $(this).attr('tabindex', 1).focus();
        $(this).toggleClass('active');
        $(this).find('.dropdown-menu').slideToggle(300);
    });
    $('.dropdown').focusout(function() {
        $(this).removeClass('active');
        $(this).find('.dropdown-menu').slideUp(300);
    });
    $('.dropdown .dropdown-menu li').click(function() {
        $(this).parents('.dropdown').find('span').text($(this).text());
        $(this).parents('.dropdown').find('input').attr('value', $(this).attr('id'));
    });
    /*End Dropdown Menu*/
})

$(document).ready(function() {
    $('.dropdown-menu li').click(function() {
        var input = '<strong>' + $(this).parents('.dropdown').find('input').val() + '</strong>',
            msg = '<span class="msg">Hidden input value: ';
        $('.msg').html(msg + input + '</span>');
    });
})


function sendSearchQuery(query, screen_size) {
    let url = '/search/'
    $.ajax({
        method: 'GET',
        url: url,
        data: {
            query: query
        },
        beforeSend: function() {},
        success: function(result) {
            drawSearchResults(result, screen_size);
        },
        error: function(response) {}
    });
}

function drawSearchResults(result, display_size) {
    let dropdown_results_div = ''

    if (display_size == 'dekstop') {
        dropdown_results_div = $('.dropdown-results')
    } else if (display_size == 'tablet') {
        dropdown_results_div = $('.dropdown-results-small')
    }

    let results_ul_html = $(dropdown_results_div).find('.list-unstyled')

    if (result.count >= 1) {
        results_ul_html.children().remove()
        $.each(result['results'], function(index, product) {
            let category_list = ''
            let category_quantity = product.categories.length

            $.each(product.categories, function(index, category_name) {
                // Если элемент первый и не единственный
                if (index == 0 && category_quantity > 1) {
                    category_list += category_name + ' / '
                } // Если элемент первый и единственный
                else if (index == 0 && category_quantity == 1) {
                    category_list += category_name
                }
                // Если элемент последний
                if (category_quantity > 1 && index == (category_quantity - 1)) {
                    category_list += category_name.toLowerCase()
                }
                // Если элемент не первый и не последний
                if (index != 0 && index != (category_quantity - 1)) {
                    category_list += category_name.toLowerCase() + ' / '
                }

            })
            let result_li_html =
                '<li>' +
                '<a href="' + product.absolute_url + '" class="links-for-search">' +
                '<div class="div-in-link">' +

                '<div class="wrapper-for-search-images">' +
                '<img src="' + product.thumbnail_url + '" class="search-img">' +
                '</div>' +

                '<div class="name-and-categorys-search">' +
                product.name +
                '<div class="categorys-for-search">' +
                category_list +
                '</div>' +
                '</div>' +

                '</div>' +
                '</a>' +
                '</li>'
            results_ul_html.append(result_li_html)
        })
        $(dropdown_results_div).css({
            'display': 'block'
        });
    } else {
        $(dropdown_results_div).css({
            'display': 'none'
        });
    }
}
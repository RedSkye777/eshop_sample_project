// variable that keeps all the filter information
let send_data = {}
let ind_filter_list = {}
let drawn_product_id_list = []
let allow_ajax_request = true
let next_page_number = 2

$(document).ready(function() {
    // check if all necessary elements are exist
    checkElementsExist();
    // reset all parameters on page load
    resetPageVariables();
    // Fill the filters data via ajax request
    getFilterData()

    // Function that calls on clicking on any individual filter checkbox
    $('.individual_filter').change(function() {
        const action = 'get_filtered_data'

        let filter_key = $(this).data('filter-key')
        let input_list = $(this).find(':input')

        $.each(input_list, function() {
            if (this.checked) {
                if (filter_key in ind_filter_list) {
                    // Повторное добавление значения одного фильтра в ind_filter_list
                    if (ind_filter_list[filter_key].includes(this.value) == false) {
                        ind_filter_list[filter_key].push(this.value)
                    }
                } else {
                    // Первое добавление фильтра в ind_filter_list
                    ind_filter_list[filter_key] = [this.value]
                }
            } else {
                if (filter_key in ind_filter_list) {
                    if (ind_filter_list[filter_key].includes(this.value) == true) {
                        remove_element(filter_key, this.value)
                    }
                }
            }
        })

        getAPIData(action)
    })

    // Function that calls on clicking on any individual filter checkbox
    $('#price-filter').change(function() {
        const action = 'get_filtered_data'
        let price_from_html = $(this).find('#price-input-from')
        let price_to_html = $(this).find('#price-input-to')

        let price_from_int = parseInt($(price_from_html).val())
        let price_to_int = parseInt($(price_to_html).val())

        if (isNaN(price_from_int)) {
            console.log('price-input-from is not found.');
            return false
        }

        if (isNaN(price_to_int)) {
            console.log('price-input-to is not found.');
            return false
        }

        send_data['price_from'] = price_from_int
        send_data['price_to'] = price_to_int

        getAPIData(action)
    })

    // Action on scrolling page
    $(window).scroll(function() {
        const action = 'load_more_products'
        let scrollTop = $(window).scrollTop()
        let windowHeight = $(window).height()
        let scrollBottom = scrollTop + windowHeight
        let product_block_children_html = $('#product-block').children()

        if (product_block_children_html.length > 1) {
            let last_product_number = $('#product-block').children().length - 1
            let last_product_html = $(product_block_children_html[last_product_number])
            let last_product_top_position_int = last_product_html.position().top

            if (scrollBottom > last_product_top_position_int) {
                getAPIData(action)
            }
        }
    })

    // Clicking on sorting options on dekstop screen
    $('#sorting-options-ul').children().click(function() {
        let action = 'get_filtered_data'
        let sort_value = $(this).data('value')
        let sort_name = $(this).text()

        $('#sorting-popularity').text('Сортировка: ' + sort_name)
        $('#sorting-popularity').attr('data-value', sort_value)

        send_data['sort_by'] = sort_value

        getAPIData(action)
    })

    // Clicking on sorting options on tablet screen
    $('#sorting-options-ul-small').children().click(function() {
        let action = 'get_filtered_data'
        let sort_value = $(this).data('value')
        let sort_name = $(this).text()

        $('#sorting-popularity').text('Сортировка: ' + sort_name)
        $('#sorting-popularity').attr('data-value', sort_value)
        $('.name-sorting-small').text(sort_name)

        send_data['sort_by'] = sort_value

        getAPIData(action)
    })
})

function getAPIData(action) {
    let url = '/product-list-by-filter/'
    let current_category = document.title

    if (action == 'get_filtered_data') {
        allow_ajax_request = true
        next_page_number = 1
        drawn_product_id_list = []
    }

    send_data.ind_filter_list = JSON.stringify(ind_filter_list)
    send_data.category = current_category
    send_data.page = next_page_number

    if (allow_ajax_request == true) {
        $.ajax({
            method: 'GET',
            url: url,
            data: send_data,
            beforeSend: function() {
            },
            success: function(result) {
                if (result.results.length < 1) {
                    allow_ajax_request = false
                } else {
                    next_page_number += 1
                }
                drawProductData(result, action);
            },
            error: function(response) {
                // if (action == 'get_filtered_data') {
                //     $("#product-block").children().remove()
                //     $("#product-block").append(
                //         $('#load-error-div').show()
                //     )
                // };

            }
        });
    }
}


// Function that resets all the page variables
function resetPageVariables() {
    ind_filter_list = {};
    send_data = {}
    drawn_product_id_list = []

    send_data.sort_by = 'popularity'
}

// Function that redraws products on the screen
function drawProductData(result, action) {

    let product_block_html = $('#product-block')
    let product_div_html = $('#product-div-for-render-js')

    if ((action == 'get_filtered_data')) {
        $('#product-block').children().remove()

        if (result.results.length < 1) {
            $('#product-block').html(
                $('#products-not-found-div').show()
            )
            return false
        } 
    }

    

    $.each(result['results'], function(a, product) {
        let feature_list_string = ''
        let workflow_div = document.createElement('div')

        $.each(product.features, function(a, feature) {
            feature_list_string += '<li>' + feature.name + ': ' + feature.value + '</li>'
        })

        $(workflow_div).attr('class', $(product_div_html).attr('class'))
        $(workflow_div).html($(product_div_html).html())
        $(workflow_div).attr('id', 'product-div-' + product.id)
        $(workflow_div).find('#product-image-url').attr('src', product.thumbnail_url)
        $(workflow_div).find('#product-name').attr('href', product.absolute_url)
        $(workflow_div).find('#product-image-abs-url').attr('href', product.absolute_url)
        $(workflow_div).find('#product-name').text(product.name)
        $(workflow_div).find('.info-list-of-products').append(feature_list_string)
        $(workflow_div).find('.current-product-price').text(parseInt(product.price) + ' ₽')
        // $(workflow_div).find('.price-of-products-old').html('<s>' + parseInt(product.price) + ' ₽' + '</s>')
        $(workflow_div).find('.btn-add-to-basket').attr('data-id', product.id)
        $(workflow_div).find('.btn-add-to-basket').attr('data-product-price', product.price)
        $(workflow_div).find('.btn-outline-secondary').attr('data-id', product.id)

        if (drawn_product_id_list.includes(product.id) == false) {
            drawn_product_id_list.push(product.id)
            $('#product-block').append(workflow_div)
        }

    });
}

function getFilterData() {
    // List of all individual filters
    let filter_array = $('.individual_filter')
    let url = '/ajax/get-filter-data/';
    let counter = 0

    $.each(filter_array, function() {
        let filter_key = $(this).attr("data-filter-key")
        let attach_to = $(this).find('ul')

        $.ajax({
            method: 'GET',
            url: url,
            data: { 'filter_key': filter_key },
            success: function(result) {
                $.each(result["response"], function(a, b) {
                    counter += 1
                    $(attach_to).append(
                        '<li class="checkbox-div">' +
                        '<div class="form-check">' +
                        '<input type="checkbox" class="form-check-input css-checkbox" id="ind-filter-' + counter + '" value="' + b[0] + '">' +
                        '<label class="form-check-label css-label" for="ind-filter-' + counter + '">' + b[0] + '</label>' +
                        '<span class="quantity-checkbox">' + ' (' + b[1] + ')' + '</span>' +
                        '</div>' +
                        '</li>'
                    )
                });
            },
            error: function(response) {
            }
        });
    })
}

// Function that removes particular element from array and deletes key that has no more values
function remove_element(key, element) {
    const index = ind_filter_list[key].indexOf(element);

    if (index > -1) {
        ind_filter_list[key].splice(index, 1);
    }

    if (ind_filter_list[key].length < 1) {
        delete ind_filter_list[key];
    }

}

// Function that checks whether all necessary elements exist in page
function checkElementsExist() {
    // Elements for redrawing the page data
    let product_block_html = $('#product-block')
    let product_div_html = $('#product-div-for-render-js')
    let product_image_url = $(product_div_html).find('#product-image-url')
    let product_name = $(product_div_html).find('#product-name')
    let product_name_href = $(product_div_html).find('#product-name')
    let product_features = $(product_div_html).find('.info-list-of-products')
    let product_current_price = $(product_div_html).find('#product-current-price')
    let product_old_price = $(product_div_html).find('.price-of-products-old')

    // Check if elements exist
    if ($(product_block_html).length < 1) {
        console.log('First element for redrawing the page data is not found.');
        return false;
    }
    if ($(product_div_html).length < 1) {
        console.log('Second element for redrawing the page data is not found.');
        return false;
    }
    if ($(product_image_url).length < 1) {
        console.log('Third element for redrawing the page data is not found.');
        return false;
    }
    if ($(product_name).length < 1) {
        console.log('Fourth element for redrawing the page data is not found.');
        return false;
    }
    if ($(product_name_href).length < 1) {
        console.log('Fifth element for redrawing the page data is not found.');
        return false;
    }
    if ($(product_features).length < 1) {
        console.log('Sixth element for redrawing the page data is not found.');
        return false;
    }
    if ($(product_current_price).length < 1) {
        console.log('Seventh element for redrawing the page data is not found.');
        return false;
    }
    if ($(product_old_price).length < 1) {
        console.log('Eigth element for redrawing the page data is not found.');
        return false;
    }

    // Price filter elements
    let price_filter_html = $('#price-filter')
    let price_from_html = $('#price-filter').find('#price-input-from')
    let price_to_html = $('#price-filter').find('#price-input-to')

    if ($(price_filter_html).length < 1) {
        console.log('First element of price filter is not found.');
        return false;
    }

    if ($(price_from_html).length < 1) {
        console.log('Second element of price filter is not found.');
        return false;
    }

    if ($(price_to_html).length < 1) {
        console.log('Third element of price filter is not found.');
        return false;
    }

    // Sorting elements
    let sort_by_html = $('#sorting-popularity')
    let sort_by_options_html = $('.sorting-popularity-menu')

    if ($(sort_by_html).length < 1) {
        console.log('First element for sorting is not found.');
        return false;
    }

    if ($(sort_by_options_html).length < 1) {
        console.log('Second element for sorting is not found.');
        return false;
    }
}
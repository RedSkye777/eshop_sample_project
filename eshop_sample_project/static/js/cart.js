$(document).ready(function () {
    // Действия при нажатии на плюс
    $('.quantity-plus').click(function (e) {
        let product_id = $(this).data('product-id')
        let action = 'plus'
    
        cart_data = getCartData(product_id, action)

        // Send data to the server and call recalculatePrice function, if successful
        sendCartData(cart_data, product_id, action)
        
    })

    // Действия при нажатии на минус
    $('.quantity-minus').click(function () {
        let product_id = $(this).data('product-id')
        let action = 'minus'
    
        cart_data = getCartData(product_id, action)

        // Send data to the server and call recalculatePrice function, if successful
        sendCartData(cart_data, product_id, action)
        
    })

    // Действия при нажатии на кнопку удаления
    $('.button-delete').click(function () {
        let product_id = $(this).data('product-id')
        let action = 'removal'
    
        cart_data = getCartData(product_id, action)

        // Send data to the server
        sendCartData(cart_data, product_id, action)
    })
})

// Function that returns cart data info
function getCartData(product_id, action) {
    let send_data = {}
    let item_id = "#product-line-" + product_id
    let item_html = $(item_id)
    let product_quantity_int = parseInt($(item_html).find('.window-quantity').val())

    if (action == 'plus') {
        send_data = {
            product_id: product_id,
            quantity: 1,
        }
    }

    if (action == 'minus') {
        send_data = {
            product_id: product_id,
            quantity: product_quantity_int - 1,
            update: 'true'
        }
    } 

    if (action == 'removal') {
        send_data = {
            product_id: product_id
        }
    } 

    return send_data
}

// Function that sends cart data to the server
function sendCartData(cart_data, product_id, action) {
    let url = '/cart/add/'

    if (action == 'removal') {
        url = '/cart/remove/'
    } 

    $.ajax({
        method: 'POST',
        url: url,
        data:cart_data,
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function (result) {
            if (result.status == 'ok') {
                redrawPageInfo(product_id, action)
            } 
        },
        error: function (response) {
        }
    });
}

// Function that recalculates and redraws prices on the page
function redrawPageInfo(product_id, action) {
    let item_id = "#product-line-" + product_id
    let item_html = $(item_id)
    let item_price_html = $(item_html).find('.window-quantity').data('product-price')
    let item_total_price_html = $(item_html).find('.product-price')
    let cart_total_price_html = $('#cart-total-price')
    let cart_total_price_miniature_html = $('#minituare-basket-total-cost')

    let item_quantity_html = $(item_html).find('.window-quantity')
    let cart_quantity_miniature_html = $('#basket-count')

    let cart_block_html = $('#main-container-basket')

    let item_price_int = parseFloat(item_price_html)
    let item_total_price_int = parseFloat(item_total_price_html.text())
    let cart_total_price_int = parseFloat(cart_total_price_html.text())
    let cart_total_price_miniature_int = parseFloat(cart_total_price_miniature_html.text())
    let item_quantity_int = parseInt(item_quantity_html.val())
    let cart_quantity_miniature_int = parseInt(cart_quantity_miniature_html.text())


    if (action == 'plus') {
        // Plusing one item-price to item-total-price, cart-total-price and cart-total-price-miniature 
        item_total_price_html.text(
            item_price_int + item_total_price_int + ' ₽'
        )

        cart_total_price_html.text(
            item_price_int + cart_total_price_int + ' ₽'
        )

        cart_total_price_miniature_html.text(
            item_price_int + cart_total_price_miniature_int + ' ₽'
        )

        // Adding plus one to item-quantity and cart-quantity-miniature
        item_quantity_html.val(
            item_quantity_int + 1
        )
        
        cart_quantity_miniature_html.text(
            cart_quantity_miniature_int + 1
        )
    } 

    if (action == 'minus') {

        // Prohibit action if the number of product is less than one
        if (item_quantity_int <= 1) {
            return false
        }

        // Minusing one item-price from item-total-price, cart-total-price and cart-total-price-miniature 
        item_total_price_html.text(
            item_total_price_int - item_price_int + ' ₽'
        )

        cart_total_price_html.text(
            cart_total_price_int - item_price_int + ' ₽'
        )

        cart_total_price_miniature_html.text(
            cart_total_price_miniature_int - item_price_int + ' ₽'
        )

        // Minusing one from item-quantity and cart-quantity-miniature
        item_quantity_html.val(
            item_quantity_int - 1
        )
        
        cart_quantity_miniature_html.text(
            cart_quantity_miniature_int - 1
        )

    } 

    if (action == 'removal') {

        if (cart_quantity_miniature_int - item_quantity_int <= 0)  {
            window.location.reload()
            return false
        }
        $(item_html).remove()

        cart_quantity_miniature_html.text(
            cart_quantity_miniature_int - item_quantity_int
        )

        cart_total_price_html.text(
            cart_total_price_int - item_total_price_int + ' ₽'
        )
        
        cart_total_price_miniature_html.text(
            cart_total_price_miniature_int - item_total_price_int + ' ₽'
        )
        
    }
}
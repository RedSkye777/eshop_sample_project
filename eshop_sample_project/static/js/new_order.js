// Комментарий к заказу
$(document).ready(function () {
    $('.comment-link').click(function () {
        if ($(".textarea-hidden").is(":visible") == true) {
            $('.textarea-hidden').fadeOut(300);
        } else {
            $('.textarea-hidden').fadeIn(300);
        }
    })
})

// Маска для ввода телефона
$(document).ready(function () {
    $(".phone-mask").mask("+7(999)-999-99-99");
});

// показать блок при выборе СДЭК
$(document).ready(function () {
    $('.label-cdek').on('click', function () {
        if ($('.cdek-delivery').prop('checked', true)) {
            $('.self-delivery-area').hide();
            $('.cdek-area').show();
        }
    })
});

// показать список точек самовывоза при нажатии на изменить пункт и изменить текст кнопки
$(document).ready(function () {
    $('.change-station-btn').on('click', function () {
        let block = $('.list-self-del-stations');
        $(this).text(block.is(':visible') ? 'Изменить пункт' : 'Свернуть');
        $('.list-self-del-stations').toggle();
    })
});

// скрыть список точек самовывоза при нажатии на изменить пункт и изменить текст кнопки
$(document).ready(function () {
    $('.change-current-station-btn').on('click', function () {
        $('.change-station-btn').text('Изменить пункт');
        $('.list-self-del-stations').hide();
    })
});

// показать блок при выборе самовывоза
$(document).ready(function () {
    $('.label-self').on('click', function () {
        if ($('.self-delivery').prop('checked', true)) {
            $('.cdek-area').hide();
            $('.self-delivery-area').show();
        }
    })
});

// показать блок при выборе СДЭК до квартиры
$(document).ready(function () {
    $('.label-cdek-to-apart').on('click', function () {
        if ($('.input-cdek-to-apart').prop('checked', true)) {
            $('.self-delivery-cdek').hide();
            $('.delivery-cdek-to-apart').show();
        }
    })
});

// Variable that keeps form data for sending
let data_for_sending = {};

// Отправка информации о заказе на сервер при нажатии "далее"
$(document).ready(function () {

    $('#my-form').submit(function (e) {
        e.preventDefault();

        formData = $('#my-form').serializeArray();
        formData = formData.reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        data_for_sending = formData;

        let address = ''
        let user_city = $('#city-input').val()
        
        if (user_city.length < 1) {
            console.log('User city is not found.');
            return false;
        } else {
            address = address + user_city + ', '
        }

        if (formData.delivery_method == 'cdek') {
            if (formData.delivery_detail == 'to_apartment') {
                let user_street = $('#street-input').val()
                let user_apartment = $('#apartment-input').val()
    
                if (user_street.length < 1) {
                    console.log('User street is not found.');
                    return false;
                } else {
                    address = address + user_street + ', '
                }
    
                if (user_apartment.length > 0) {
                    address = address + user_apartment
                } 
            } 
            else if (formData.delivery_detail == 'to_pickup_point') {
                let pickup_point_address = $('#pickup-point-address').text()
    
                if (pickup_point_address.length < 1) {
                    console.log('Pickup point address is not found.');
                    return false;
                } else {
                    address = address + pickup_point_address
                }
            } else {
                console.log('Delivery detail option does not exist or is not found.');
                return false;
            }
        } else if (formData.delivery_method == 'pickup') {
            let shop_pickup_address = $('#shop-pickup-address').text()

            if (shop_pickup_address.length < 1) {
                console.log('Shop address is not found.');
                return false;
            } else {
                address = address + shop_pickup_address
            }
        }
    
        data_for_sending['delivery_address'] = address
        data_for_sending['delivery_tariff'] = '23'

        // отправка данных при нажатии на кнопку
        sendFormData()
    });
})

function sendFormData() {
    let url = '/order/new-order/'

    $.ajax({
        method: 'POST',
        url: url,
        data: data_for_sending,
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }

        },
        success: function (result) {
            if (result.status == 'ok') {
                let redirect_to = result.redirect_to
                window.location.replace(redirect_to)
            }

        },
        error: function (response) {
        }
    });
}

// Настройка autocomplete-input для поля ввода населённого пункта
$(document).ready(function () {

    $('#city-input').autocomplete({
        serviceUrl: '/delivery/get-city-prompt/',
        onSearchStart: function (input_value) {
            redrawPageData('none', 'search-for-city-prompt-start')
        },
        beforeRender: function(container, suggestions) {
        },
        formatResult: function (suggestion, currentValue) {
            return '<div class="links-for-city" data-cdek-city-id="' + suggestion.city_id + '">' + suggestion.value + '</div>'
        },
        onSelect: function (suggestion) {
            $('#city-input').attr('data-cdek-city-id', suggestion.city_id)

            // Подгружаем список пунктов выдачи заказа сдэк
            sendDeliveryData({cdek_city_id: suggestion.city_id}, 'cdek-pick-point-options-load')
            
            redrawPageData('none', 'city-prompt-selected')
        },
        onSearchComplete: function (query, suggestions) {
            redrawPageData(suggestions, 'search-for-city-prompt-complete')
        }
    });

    // При выборе пунктов доставки СДЭК
    $('.label-self-del-cdek').click(function () {
        $('.input-self-del-cdek').prop('checked', true)
        $('#street-input').prop('required', false);

        $('.delivery-cdek-to-apart').hide()
        $('.self-delivery-cdek').show()
        
    })

    // При выборе варианта доставки до квартиры
    $('.label-cdek-to-apart').click(function () {
        $('.input-cdek-to-apart').prop('checked', true)
        $('#street-input').prop('required', true);

        $('.self-delivery-cdek').hide()
        $('.delivery-cdek-to-apart').show()
    })


})

// Функция, отрисовывающая новые данные на странице в зависимости от поставленной задачи.
function redrawPageData(data, action) {

    // Начало поиска населённого пункта
    if (action == 'search-for-city-prompt-start') {
        $('#order-creation-button').attr("disabled", true);
        $('#delivery-details-block').fadeOut()
    }

    // Выбор населённого пункта из списка предложенных
    else if (action == 'city-prompt-selected') {
        $('#city-input').removeClass('is-invalid')
        $('.address-error').hide()

        $('#order-creation-button').attr("disabled", false);
        $('#delivery-details-block').fadeIn()
    }

    // Конец поиска населённого пункта
    else if (action == 'search-for-city-prompt-complete') {
        if (data.length < 1) {
            $('#city-input').addClass('is-invalid')
            $('.address-error').show()
        } else {
            $('#city-input').removeClass('is-invalid')
            $('.address-error').hide()
        }
    }

    // Рендерим список пунктов выдачи заказа СДЭК
    else if (action == 'cdek-pick-point-options-load') {
        if (data.pvz_list.length > 0) {
            let current_delivery_point = $('.name-current-self-del-station')
            let delivery_point_dropmenu = $('.list-self-del-stations')

            $(current_delivery_point).text(
                data.pvz_list[0].address
            )

            $(delivery_point_dropmenu).children().remove()

            if (data.pvz_list.length == 1) {
                $('#change-pvz-button').hide()
            } else {
                $('#change-pvz-button').show()
                $.each(data.pvz_list, function(index, pvz) {
                    if (index > 0) {
                        $(delivery_point_dropmenu).append(
                            '<div class="self-del-station">' + 
                                '<div class="name-potential-self-del-station">' + pvz.address + '</div>' +
                                '<button type="button" class="btn change-current-station-btn btn-outline-secondary">Выбрать</button>' +
                            '</div>'
                        )
                    }
                })
            }

        }
    }

    // Действие при нажатии на "Доставка в пункт самовывоза СДЭК"
    else if (action == 'cdek-pick-point-option-cliked') {
        if (data.pvz_list.length > 0) {
            let current_delivery_point = $('.name-current-self-del-station')
            let delivery_point_dropmenu = $('.list-self-del-stations')
            let delivery_point_quantity = 1

            $(current_delivery_point).text(
                data.pvz_list[0].address
            )
            
            $(delivery_point_dropmenu).children().remove()

            if (data.pvz_list.length == 1) {
                $('#change-pvz-button').hide()
            } else {
                $('#change-pvz-button').show()
                $.each(data.pvz_list, function(index, pvz) {
                    if (index > 0) {
                        $(delivery_point_dropmenu).append(
                            '<div class="self-del-station">' + 
                                '<div class="name-potential-self-del-station">' + pvz.address + '</div>' +
                                '<button type="button" class="btn change-current-station-btn btn-outline-secondary">Выбрать</button>' +
                            '</div>'
                        )
                    }
                })
            }

            $('.delivery-cdek-to-apart').hide();
            $('.self-delivery-cdek').show();
        }
    }

    // При запросе стоимости доставки
    else if (action == 'get-shipping-cost') {
        $('.delivery-price-area').text(
            'Стоимость доставки:' + data.price + '₽'
        )
        $('.delivery-price-area').show()

        $('#delivery-max-date').text(
            data.deliveryDateMax
        )
        $('#delivery-cost').text(
            data.price
        )
    }
}


// Функция, отрисовывающая loader в соответственных местах страницы. 
function drawLoader(action) {
    let loader = $('#ajax-loader')

    if ($(loader).css('display') == 'block' ) {
        return false
    }

    if (action == 'validate-city-input') {
        $('#delivery-details-block').hide()

        $('#delivery-details-block').before(loader)
        $('#ajax-loader').show()
    } 
    else if (action == 'search-for-city-prompt') {
        $('#delivery-details-block').hide()

        $('#delivery-details-block').before(loader)
        $('#ajax-loader').show()
    }
    
}

// Универсальная функция для отправки данных на сервер
function sendDeliveryData(data, action) {
    let url = ''

    if (action == 'cdek-pick-point-options-load') {
        url = '/delivery/get-cdek-delivery-points/'
    }
    else if (action == 'get-shipping-cost') {
        url = '/delivery/get-cdek-shipping-cost/'
    }
    else {
        console.log('action is not defined');
        return false;
    }
    
    $.ajax({
        method: 'GET',
        url: url,
        data: data,
        beforeSend: function () {
            drawLoader(action)
        },
        success: function (result) {
            redrawPageData(result, action)
        },
        error: function (response) {
        }
    });
}
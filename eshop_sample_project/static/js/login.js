$(document).ready(function() {
    let redirect_to = get_url_param('next')
    
    if (redirect_to == null) {
        return false
    } else {
        let current_form_action = $('#authentication-form').attr('action')
        let replacy_with = current_form_action + '?next=' + redirect_to

        $('#authentication-form').attr('action', replacy_with)
    }
    
})

function get_url_param(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}
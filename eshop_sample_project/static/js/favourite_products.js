$(document).ready(function() {
    $(document).on('click', '.add-to-cart', function(e) {
        e.preventDefault();
        let product_id = $(this).attr('data-product-id')

        let send_data = {
            product_id: product_id,
            quantity: 1
        }

        sendPageData(send_data, 'add-to-cart', product_id)
    })

    $(document).on('click', '.remove-from-favourites', function(e) {
        e.preventDefault();
        let product_id = $(this).attr('data-product-id')

        let send_data = {
            product_id: product_id,
            action: 'remove-product'
        }

        sendPageData(send_data, 'remove-product', product_id)
    })
})

function sendPageData(send_data, action, product_id) {
    let url = ''

    if (action == 'remove-product') {
        url = '/account/favorites/'
    } else if (action == 'add-to-cart') {
        url = '/cart/add/'
    } else {
        console.log('Action is not defined.');
        return false;
    }

    $.ajax({
        method: 'POST',
        url: url,
        data: send_data,
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
            console.log(send_data);
        },
        success: function(result) {
            console.log(result);
            redrawPageData(result, action, product_id)
        },
        error: function(response) {}
    });
}

function redrawPageData(result, action, product_id) {
    if (action == 'remove-product') {
        if (result.errors.length < 1) {
            let favourite_block_html = $('#favourite-products-div')

            $('#favourite-product-div-' + product_id).remove()

            if ($(favourite_block_html).children().length < 1) {
                $(favourite_block_html).html(
                    '<div class="empty-profile-block">' + 
                    '<h1 class="caption-empty-profile">Список избранного пуст.</h1>' +
                    '<p class="picture-empty-profile"><i class="far fa-heart"></i></p>' +
                    '<p class="text-empty-profile">Индивидуальный подбор товара по телефону (бесплатно по России):</p>' +
                    '<span class="tel-empty-profile">8 800 700 15 00</span>' +
                    '</div>'
                )
            }
        }
    } else if (action == 'add-to-cart') {
        if (result.status == 'ok') {
            let product_price_html = $('#favourite-product-div-' + product_id).find('.price-item-fav')
            let add_to_cart_button = $('#add-to-cart-button-' + product_id)
            let basket_miniature_html = $('#basket-form')
            let basket_detail_url = '/cart/'
            let cart_length_html = $('#basket-count')
            let cart_total_cost_html = $(basket_miniature_html).find('#minituare-basket-total-cost')

            let product_price_int = parseInt($(product_price_html).text())
            let cart_length_int = parseInt($('#basket-count').text())
            let cart_total_cost_int = parseInt($(basket_miniature_html).find('#minituare-basket-total-cost').text())

            let updated_cart_length_int = 1
            let updated_cart_total_cost_int = product_price_int

            if (!isNaN(cart_length_int)) {
                updated_cart_length_int = (cart_length_int + 1)
            }

            if (!isNaN(cart_total_cost_int)) {
                updated_cart_total_cost_int = (cart_total_cost_int + product_price_int)
            }

            // Изменяем внешний вид и свойства миниатюры-корзины
            basket_miniature_html.children().remove()
            basket_miniature_html.append(
                '<a href="' + basket_detail_url + '">' +
                '<div id="basket-area" class="btn btn-danger">' +
                '<div class="cart-area">' +
                '<ion-icon class="icon-cart-large" name="cart"></ion-icon>' +
                '<span class="span-for-cart-quantity"><span id="basket-count">' + updated_cart_length_int + '</span></span>' +
                '</div>' +

                '<div class="count-area">' +
                '<span id="minituare-basket-total-cost">' + updated_cart_total_cost_int + ' р.' + '</span>' +
                '</div>' +
                '</div>' +
                '</a>'
            )

            // Меняем кнопку добавления товара в корзину
            $(add_to_cart_button).addClass("active-add-to-cart");
        }
    }
}
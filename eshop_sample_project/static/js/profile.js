$(document).ready(function() {
    $(".phone-mask").mask("+7(999)-999-99-99");
});

// при нажатии на Редактировать показывается блок с изменяемой инфой
$(".edit-personal-info-btn").click(function() {
    $(".contact-detail-area").hide();
    $(".hidden-detail-area").show();
});

// при нажатии на Отменить скрывается блок с изменяемой инфой
$(".cancel-personal-info-btn").click(function() {
    $(".hidden-detail-area").hide();
    $(".contact-detail-area").show();
});


$(document).ready(function() {
    $('#save-changes-button').click(function(e) {
        e.preventDefault();

        let name = $('#name-change-input').val()
        let last_name = $('#surname-change-input').val()
        let phone_number = $('#phone-change-input').val()

        let send_data = {
            first_name: name,
            last_name: last_name,
            phone_number: phone_number
        }

        sendProfileData(send_data, 'update-profile-data')

    })
})

function sendProfileData(send_data, action) {
    let url = ''

    if (action == 'update-profile-data') {
        url = '/account/update-user-data/'
    }

    $.ajax({
        method: 'POST',
        url: url,
        data: send_data,
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }

        },
        success: function(result) {

            redrawPageInfo(result, action)
        },
        error: function(response) {}
    });

}

function redrawPageInfo(data, action) {

    if (action == 'update-profile-data') {
        if (data.status == 'ok') {
            let name = $('#name-input')
            let last_name = $('#surname-input')
            let phone_number = $('#phone-input')

            let updated_name = $('#name-change-input').val()
            let updated_last_name = $('#surname-change-input').val()
            let updated_phone_number = $('#phone-change-input').val()

            $(name).val(updated_name)
            $(last_name).val(updated_last_name)
            $(phone_number).val(updated_phone_number)

            $(".hidden-detail-area").hide();
            $(".contact-detail-area").show();


        } else {}
    }

}
import os.path
import shutil

from django.test import TestCase, override_settings
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings

from main import models

class TestMainModels(TestCase):

    def test_category_model_methods(self):

        # Test auto slug creation
        category = models.Category.objects.create(
            name='Сухофрукты',
        )

        self.assertEqual(
            category.slug,
            'sukhofrukty'
        )

        # Test slug changes if name changes
        category.name = 'Масло'
        category.save()

        self.assertEqual(
            category.slug,
            'maslo'
        )

        # Test custom validate_unique method
        with self.assertRaises(ValidationError):
            models.Category.objects.create(
                name='Масло',
            )
        
        # Test category name update works
        category.name = 'Масло'
        category.save()
        
        # Test get_products_quantity method
        # With no products
        self.assertEqual(
            models.Category.objects.create(name='Манты').get_products_quantity(),
            0
        )

        # Test get_absolute_url method
        category = models.Category.objects.create(
            name='Масла',
        )
        response = self.client.get(category.get_absolute_url())

        self.assertEqual(
            response.status_code,
            200
        )

    def test_product_brand_model_methods(self):

        # Test custom save method
        brand = models.ProductBrand.objects.create(
            name='Створки',
        )

        self.assertEqual(
            brand.slug,
            'stvorki'
        )

         # Test slug changes if name changes
        brand.name = 'Apple'
        brand.save()

        self.assertEqual(
            brand.slug,
            'apple'
        )

        # Test custom validate_unique method
        with self.assertRaises(ValidationError):
            models.ProductBrand.objects.create(
                name='Apple',
            )
    
    @override_settings(MEDIA_ROOT='test_data/media/')
    def test_product_image_model(self):

        # Test imageKit package creates thumbnail
        # Firstly, assert that test product image exists

        test_image_path = 'fixtures/product_image.jpg'

        self.assertTrue(
            os.path.exists(
                test_image_path
            )
        )

        new_image = models.ProductImage.objects.create(
            image=SimpleUploadedFile(name='image_for_test', content=open(test_image_path, 'rb').read(), content_type='image/jpeg')
        )
        self.assertEqual(
            models.ProductImage.objects.all().count(),
            1
        )

        self.assertTrue(
            new_image.thumbnail
        )

        shutil.rmtree('test_data/media/', ignore_errors=True)

        self.assertFalse(
            os.path.exists(
                'test_data/media/'
            )
        )
    
    @override_settings(MEDIA_ROOT='test_data/media/')
    def test_product_methods(self):

        # Test custom save method
        product = models.Product.objects.create(
            name='Сироп',
            price=10
        )

        self.assertEqual(
            product.slug,
            'sirop'
        )

        # Test slug lingers if object is saved with the same name
        product.name = 'Сироп'
        product.save()

        self.assertEqual(
            product.slug,
            'sirop'
        )
        
        # Test slug changes if name changes
        product.name = 'Apple'
        product.save()

        self.assertEqual(
            product.slug,
            'apple'
        )

        # Test custom validate_unique method
        with self.assertRaises(ValidationError):
            models.Product.objects.create(
                name='Apple',
                price=20
            )
        
        # Test get_public_image_url

            # Test image-not-found.png exist
        self.assertTrue(
            os.path.exists(
                'staticfiles/images/image-not-found.png'
            )
        )

        product = models.Product.objects.create(
            name='Bicycle',
            price=3000
        )
            #  Get image-not-found.png if product does not have any images
        self.assertEqual(
            product.get_public_image_url(),
            '/staticfiles/images/image-not-found.png'
        )

            #  Get first image url if products have one or more images
        first_image_path = 'fixtures/bicycle_1.webp'
        second_image_path = 'fixtures/bicycle_2.webp'

        first_image = models.ProductImage.objects.create(
            product=product,
            image=SimpleUploadedFile(name='bicycle_1', content=open(first_image_path, 'rb').read(), content_type='image/jpeg')
        )

        second_image = models.ProductImage.objects.create(
            product=product,
            image=SimpleUploadedFile(name='bicycle_2', content=open(second_image_path, 'rb').read(), content_type='image/jpeg')
        )

        self.assertEqual(
            models.ProductImage.objects.all().count(),
            2
        )

        self.assertEqual(
            product.get_public_image_url(),
            '/media/product-images/bicycle_1.jpg'
        )

        shutil.rmtree('test_data/media/', ignore_errors=True)
    
        # Test get features method
        product = models.Product.objects.create(
            name='PanCake',
            price=30
        )
        
        first_feature = models.ProductFeature.objects.create(
            product=product,
            name='delicious_level',
            value='one hundred percent'
        )

        second_feature = models.ProductFeature.objects.create(
            product=product,
            name='bake_time',
            value='5 minutes'
        )

        self.assertEqual(
            len(product.get_features()),
            2
        )
        
        # Test get_absolute_url method
        product = models.Product.objects.create(
            name='Каша',
            price=12
        )
        response = self.client.get(product.get_absolute_url())

        self.assertEqual(
            response.status_code,
            200
        )

    def test_active_manager_works(self):

        models.Product.objects.create(
            name='Coke',
            price=10,
        )
        models.Product.objects.create(
            name='Ball',
            price=120,
        )
        models.Product.objects.create(
            name='Car',
            price=1220,
            active=False,
        )
        self.assertEqual(
            models.Product.objects.active().count(),
            2
        )

    def test_product_feature_methods(self):
        product = models.Product.objects.create(
            name='Apple',
            price=12,
            number_in_stock=20
        )

        # Testing first successful feature creation
        feature = models.ProductFeature.objects.create(
            product=product,
            header='Общие характеристики',
            name='Материал корпуса',
            value='Дерево'
        )

        self.assertEqual(
            models.ProductFeature.objects.all().count(),
            1
        )

        # Testing successful feature update
        feature.header = 'Общие характеристики'
        feature.name = 'Материал корпуса'
        feature.value = 'титан'
        feature.save()

        self.assertEqual(
            models.ProductFeature.objects.get(id=feature.id).value,
            'титан'
        )

        # Testing creation feature with the same values, but different product
        product_2 = models.Product.objects.create(
            name='Сoke',
            price=20,
            number_in_stock=40
        )

        feature = models.ProductFeature.objects.create(
            product=product_2,
            header='общие характеристики',
            name='материал корпуса',
            value='титан'
        )

        self.assertEqual(
            models.ProductFeature.objects.all().count(),
            2
        )

        # Test capitalize strings works

        self.assertTrue(
            feature.header == 'Общие характеристики',
        )
        
        self.assertTrue(
            feature.name == 'Материал корпуса',
        )
    
    def test_category_feature_methods(self):
        category = models.Category.objects.create(
            name='Fruits',
        )
        product_1 = models.Product.objects.create(
            name='Apple',
            price=12,
            number_in_stock=20
        )
        product_2 = models.Product.objects.create(
            name='Pineapple',
            price=32,
            number_in_stock=20
        )

        product_1.categories.add(category)
        product_2.categories.add(category)

        # Test validate_unique method passing
        models.CategoryFeature.objects.create(
            category=category,
            header='Общие характеристики',
            name='Материал корпуса',
            value='Красная роза'
        )

        self.assertEqual(
            models.CategoryFeature.objects.all().count(),
            1
        )

        # Test validate_unique method raising validation error
        with self.assertRaises(ValidationError):
            models.CategoryFeature.objects.create(
                category=category,
                header='Общие характеристики',
                name='Материал корпуса',
                value='Красная роза'
            )

        self.assertEqual(
            models.CategoryFeature.objects.all().count(),
            1
        )

        # Test create_product_feature method already created one feature per product
        self.assertEqual(
            product_1.features.all().count(),
            1
        )
        self.assertEqual(
            product_2.features.all().count(),
            1
        )

        # Test create_product_feature method creating another product features
        models.CategoryFeature.objects.create(
            category=category,
            header='Общие характеристики',
            name='Цвет',
            value='Белый'
        )

        self.assertEqual(
            models.CategoryFeature.objects.all().count(),
            2
        )

        self.assertEqual(
            product_1.features.all().count(),
            2
        )
        self.assertEqual(
            product_2.features.all().count(),
            2
        )



        





        


    
    
    

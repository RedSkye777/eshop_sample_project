from django.test import TestCase
from django.urls import reverse
from django.core import mail
from django.conf import settings

from main import models, forms

class TestMainViews(TestCase):
    
    def test_home_page(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(
            response.status_code,
            200
        )
        self.assertTemplateUsed(
            response,
            'main/home.html'
        )
    
    def about_us_page(self):
        response = self.client.get(reverse('about_us'))
        self.assertEqual(
            response.status_code,
            200
        )
        self.assertTemplateUsed(
            response,
            'main/about_us.html'
        )
    
    def contact_us_page(self):
        response = self.client.get(reverse('contact_us'))
        self.assertEqual(
            response.status_code,
            200
        )
        self.assertTemplateUsed(
            response,
            'main/contact_us.html'
        )
        self.assertIsInstance(
            response.context['form'],
            forms.ContactForm
        )
        # Test email send 
        form_data = {
            'email': 'test@gmail.com',
            'message': 'abcabcabc'
        }
        response = self.client.post(reverse('contact_us'), data=form_data)

        assert len(mail.outbox) == 1 
        assert mail.outbox[0].subject == 'Кто-то захотел связаться с Вами.'


    def test_product_list_view(self):
        # test page with no products
        response = self.client.get(reverse('product_list'))
        self.assertEqual(
            response.status_code,
            200
        )
        self.assertEqual(
            len(response.context['product_list']),
            0
        )
        self.assertTemplateUsed(
            response,
            'main/product_list.html'
        )

        # page with some products
        product_1 = models.Product.objects.create(
            name='Bread',
            price='2'
        )
        product_2 = models.Product.objects.create(
            name='Butter',
            price='5'
        )
        response = self.client.get(reverse('product_list'))
        self.assertEqual(
            response.status_code,
            200
        )
        self.assertEqual(
            len(response.context['product_list']),
            2
        )
        # get products of existing category 
        category = models.Category.objects.create(
            name='к чаю'
        )

        product_1.categories.add(category)
        product_2.categories.add(category)

        response = self.client.get(reverse('product_list_by_category', kwargs={'category': category.slug}))
        self.assertEqual(
            response.status_code,
            200
        )
        self.assertEqual(
            len(response.context['product_list']),
            2
        )
        # get products of non existing category 
        response = self.client.get(reverse('product_list_by_category', kwargs={'category': 'unexisting_category'}))
        self.assertEqual(
            response.status_code,
            404
        )
    
    def test_product_detail_view(self):
        product = models.Product.objects.create(
            name='Масло',
            price='2'
        )

        response = self.client.get(reverse('product_detail', kwargs={
            'pk': product.pk,
            'slug': product.slug
        }))

        self.assertEqual(
            response.status_code,
            200
        )
        self.assertTemplateUsed(
            response,
            'main/product_detail.html'
        )
    
    def test_set_user_city_view(self):
        # test ajax_required decorator works
        response = self.client.post(reverse('set_user_city'), data={'status': 'Ok'})

        self.assertEqual(
            response.status_code,
            400
        )
        # test saving user city to session

        ajax_data = {'status': 'Sendind user city', 'city': 'Moscow'}
        response = self.client.post(reverse('set_user_city'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})

        self.assertEqual(
            response.status_code,
            200
        )

        self.assertEqual(
            response.wsgi_request.session[settings.CITY_SESSION_ID],
            'Moscow'
        )

        # test status error

        ajax_data = {'city': 'Moscow'}
        response = self.client.post(reverse('set_user_city'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ko'
        )

        self.assertEqual(
            response.json()['error'],
            'Status is not found.'
        )

        # test setting the same city

        ajax_data = {'status': 'Sendind user city', 'city': 'Moscow'}
        response = self.client.post(reverse('set_user_city'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ko'
        )

        self.assertEqual(
            response.json()['error'],
            'City has been already set.'
        )
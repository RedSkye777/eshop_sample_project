from django.test import TestCase
from django.urls import reverse

class TestContextProcessors(TestCase):

    def user_city_processor(self):

        # test finding no city in session
        response = self.client.get(reverse('home'))

        self.assertEqual(
            response.status_code,
            200
        )

        self.assertEqual(
            response.context['user_city'],
            'Ваш город'
        )

        # test getting updated city
        ajax_data = {'status': 'Sendind user city', 'city': 'London'}
        response = self.client.post(reverse('set_user_city'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})

        response = self.client.get(reverse('home'))

        self.assertEqual(
            response.status_code,
            200
        )

        self.assertEqual(
            response.context['user_city'],
            'London'
        )
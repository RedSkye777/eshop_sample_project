from django.test import TestCase
from django.conf import settings
from django.core import mail
from django.urls import reverse

from main import forms, models

class TestMainForms(TestCase):

    def test_contact_form(self):

        error_cases = {
            'email_missed': {
                'message': 'abcabcabc',
            },
            'message_missed': {
                'email':'test@gmail.com',
            },
        }

        # Testing validation of possible errors
        for error_code in error_cases:
            form = forms.ContactForm(data=error_cases[error_code])

            self.assertFalse(form.is_valid())
            self.assertEqual(
                len(form.errors),
                1
            )
            self.assertEqual(
                form.non_field_errors().as_data()[0].code,
                error_code
            )
        
        # Testing send_email method
        form_data = {
            'email': 'test@gmail.com',
            'message': 'abcabcabc'
        }
        form = forms.ContactForm(data=form_data)

        self.assertTrue(
            form.is_valid()
        )

        form.send_mail()

        assert len(mail.outbox) == 1 
        assert mail.outbox[0].subject == 'Кто-то захотел связаться с Вами.'
        assert mail.outbox[0].from_email == settings.PROJECT_EMAIL_ADDRESS
        assert mail.outbox[0].to == [settings.PROJECT_CUSTOMER_SERVICE_EMAIL_ADDRESS]
    
    def test_set_user_city_form(self):

        error_cases = {
            'status_missed': {
                'city': 'Moscow',
            },
            'city_missed': {
                'status':'Sending user city',
            }
        }

        # Testing validation of main possible errors
        for error_code in error_cases:
            form = forms.SetUserCityForm(data=error_cases[error_code])

            self.assertFalse(form.is_valid())
            self.assertEqual(
                len(form.errors),
                1
            )
            self.assertEqual(
                form.non_field_errors().as_data()[0].code,
                error_code
            )
    
    def test_product_feature_creation_form(self):

        # Testing successful form validation
        product = models.Product.objects.create(
            name='Apple',
            price=12,
            number_in_stock=200
        )
        correct_data = {
            'product': product.pk,
            'header': 'общие характеристики',
            'name': 'материал корпуса',
            'value': 'Пластик'
        }
        form = forms.ProductFeatureCreationForm(data=correct_data)

        self.assertTrue(form.is_valid())

        feature = form.save()

        self.assertEqual(
            models.ProductFeature.objects.all().count(),
            1
        )

        # Testing form validation error and capitalize string
        correct_data = {
            'product': product.pk,
            'header': 'общие характеристики',
            'name': 'материал корпуса',
            'value': 'Пластик'
        }
        form = forms.ProductFeatureCreationForm(data=correct_data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            'feature_already_exists'
        )
    
    def test_category_feature_creation_form(self):

        # Testing successful form validation
        category = models.Category.objects.create(
            name='Fruits',
        )
        correct_data = {
            'category': category.pk,
            'header': 'общие характеристики',
            'name': 'материал корпуса',
            'value': 'Пластик'
        }
        form = forms.CategoryFeatureCreationForm(data=correct_data)

        self.assertTrue(form.is_valid())

        feature = form.save()

        self.assertEqual(
            models.CategoryFeature.objects.all().count(),
            1
        )

        # Testing form validation error and capitalize string
        correct_data = {
            'product': category.pk,
            'header': 'общие характеристики',
            'name': 'материал корпуса',
            'value': 'Пластик'
        }
        form = forms.CategoryFeatureCreationForm(data=correct_data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            len(form.errors),
            1
        )
            
    
        
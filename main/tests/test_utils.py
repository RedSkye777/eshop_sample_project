from django.test import TestCase
from django.urls import reverse

from main.utils import get_form_errors
from main.forms import ContactForm

class TestMainUtils(TestCase):

    def test_get_form_errors_function(self):

        error_cases = {
            'email_not_found': {
                '': ''
            },
            'incorrect_email_message_not_found': {
                'email': 'abvsdggs',
            }
        }

        form = ContactForm(data=error_cases['email_not_found'])

        self.assertFalse(
            form.is_valid()
        )

        error_list = get_form_errors(form)

        self.assertEqual(
            len(error_list),
            1
        )

        form = ContactForm(data=error_cases['incorrect_email_message_not_found'])

        self.assertFalse(
            form.is_valid()
        )

        error_list = get_form_errors(form)

        self.assertEqual(
            len(error_list),
            2
        )





from django.conf import settings
from django.db.models import F, Prefetch, Count

from main.models import Category

def user_city(request):

    """Инициализация города"""    

    session = request.session    
    city = session.get(settings.CITY_SESSION_ID)    

    if not city:        
        return {'user_city': 'Ваш город'}
    return {'user_city': session.get(settings.CITY_SESSION_ID)}

def catalog(request):
    """Возвращаем каталог магазина, состоящий из основных категорий."""
    queryset = Category.objects.filter(level=0).prefetch_related(Prefetch('children')) \
    .annotate(products_quantity=Count(F('products')))                                    \
    .order_by('-name')

    return {'category_list': queryset}

    
    
    


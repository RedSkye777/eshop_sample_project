import uuid

from slugify import slugify
from mptt.models import MPTTModel, TreeForeignKey
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.db.models import ObjectDoesNotExist
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.conf import settings


class ActiveManager(models.Manager):
    def active(self):
        return self.filter(active=True)


class Category(MPTTModel):
    parent = TreeForeignKey(
        'self', on_delete=models.PROTECT, null=True, blank=True, 
        related_name='children', verbose_name='Родительская категория'
    )
    name = models.CharField(max_length=50, verbose_name='Название категории')
    slug = models.SlugField(max_length=50, blank=True, verbose_name='Вид в браузере')
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    active = models.BooleanField(default=True, verbose_name='Категория активна',
                                help_text='Если выключено, то все товары этой категории будут скрыты')

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = 'Категория товаров'
        verbose_name_plural = 'Категории товаров'

    def __str__(self):
        return self.name

    def validate_unique(self, exclude=None):
        slug = slugify(self.name, to_lower=True)

        # Проверяем, что идиентичной категории не существует.
        try:
            category = Category.objects.get(slug=slug)
            if category.id != self.id:
                raise ValidationError({'name': ['Категория с таким названием уже существует.']})
        except models.ObjectDoesNotExist:
            pass
    
    def save(self, *args, **kwargs):
        self.validate_unique()
        self.slug = slugify(self.name, to_lower=True)
        super().save(*args, **kwargs)

    def get_products_quantity(self):
        try:
            return self.products.count()
        except AttributeError:
            return 0
    
    def get_absolute_url(self):
        return reverse('product_list_by_category', kwargs={'category_slug': self.slug})


class ProductBrand(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название бренда')
    slug = models.CharField(max_length=100, blank=True, verbose_name='Вид в строке браузера')

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'

    def __str__(self):
        return self.name
    
    def validate_unique(self, exclude=None):
        slug = slugify(self.name, to_lower=True)

        if ProductBrand.objects.filter(slug=slug).exists():
            raise ValidationError({'name': ['Бренд с таким названием уже существует.']})
        
    def save(self, *args, **kwargs):
        self.validate_unique()
        self.slug = slugify(self.name, to_lower=True)
        super().save(*args, **kwargs)


class Product(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        blank=True
    )
    name = models.CharField(
        max_length=100,
        verbose_name='Наименование'
    )
    brand = models.ForeignKey(
        ProductBrand, 
        on_delete=models.DO_NOTHING, 
        related_name='products', 
        verbose_name='Бренд', 
        blank=True, 
        null=True
    )
    description = RichTextUploadingField(
        blank=True, 
        default='Описание отсутствует...',
        verbose_name='Описание'
    )
    price = models.DecimalField(
        max_digits=50, 
        decimal_places=2, 
        verbose_name='Цена',
    )
    slug = models.SlugField(
        max_length=100, 
        verbose_name='Вид в строке браузера',
        blank=True
    )
    active = models.BooleanField(
        default=True, 
        verbose_name='Товар активен',
        help_text='Показывать ли данный товар на сайте'
    )
    number_in_stock = models.PositiveIntegerField(
        verbose_name='Колличество товара в наличии',
        default=1,
    )
    number_reserved = models.IntegerField(
        verbose_name='Зарезервировано в заказах',
        default=0,
        blank=True
    )
    number_purchased = models.IntegerField(
        verbose_name='Колличесво приобретений',
        default=0,
        blank=True
    )
    weight = models.PositiveIntegerField(
        verbose_name='Вес товара (граммы)',
        default=1
    )
    length = models.PositiveIntegerField(
        verbose_name='Длина товара (см)',
        default=1
    )
    width = models.PositiveIntegerField(
        verbose_name='Ширина товара (см)',
        default=1
    )
    height = models.PositiveIntegerField(
        verbose_name='Высота товара (см)',
        default=1
    )
    volume = models.PositiveIntegerField(
        verbose_name='Объём товара (м3)',
        help_text='Используется при расчёте стоимости отправки товара. ',
        blank=True,
        null=True
    )

    date_updated = models.DateTimeField(
        auto_now=True
    )
    categories = models.ManyToManyField(
        Category, blank=True, 
        verbose_name='Связанные категории',
        related_name='products'
    )
    image = ProcessedImageField(
        upload_to='product-main-images',
        format='JPEG',
        options={'quality': 50},
        blank=True,
        null=True,
        verbose_name='Основное изображение',
    )
    objects = ActiveManager()

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name
    
    def validate_unique(self, exclude=None):
        slug = slugify(self.name, to_lower=True)

        try:
            if not (self.slug == slug):
                if Product.objects.filter(slug=slug).exists():
                    raise ValidationError({'name': ['Товар с таким названием уже существует.']})
        except AttributeError:
            if Product.objects.filter(slug=slug).exists():
                raise ValidationError({'name': ['Товар с таким названием уже существует.']})
    
    def save(self, *args, **kwargs):
        self.validate_unique()
        self.slug = slugify(self.name, to_lower=True)
        super().save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse('product_detail', kwargs={'pk': self.pk, 'slug': self.slug})
    
    def get_features(self):
        try:
            all_features = self.features.active()
            return all_features
        except AttributeError:
            return None
    
    def get_category_tree(self):
        try:
            queryset = self.categories.all()

            if queryset.exists():
                leaf_node = queryset.first()

                for category in queryset:
                    if category.is_leaf_node():
                        leaf_node = category
                
                category_tree = leaf_node.get_ancestors(include_self=True)

                return category_tree
                    
            else:
                return []

        except AttributeError:
            return []
    
    def get_default_image_url(self):
        return '{}images/image-not-found.png'.format(settings.STATIC_URL
        )
    
class ProductExtraImage(models.Model):
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name='extra_images',
        blank=True,
        null=True
    )
    image = ProcessedImageField(
        upload_to='product-images',
        format='JPEG',
        options={'quality': 50},
        verbose_name='Основное фото'
    )

    class Meta:
        verbose_name = 'Дополнительное изображение товара'
        verbose_name_plural = 'Дополнительные изображения товара'

    
class ProductFeature(models.Model):
    """Характеристики товара"""
    product = models.ForeignKey(
        Product, 
        on_delete=models.CASCADE, 
        related_name='features',
        verbose_name='Товар',
        help_text='Присваивает текущую характеристику выбранному товару',
    )
    header = models.CharField(
        max_length=100, 
        verbose_name='Заголовок',
        help_text='Заголовок характеристики на странице с товаром')
    name = models.CharField(max_length=100, verbose_name='Наименование')
    value = models.CharField(max_length=100, verbose_name='Значение')
    active = models.BooleanField(
        default=True, 
        verbose_name='Показывать',
        help_text='Если неактивно, cкрывает данную характеристику')
    objects = ActiveManager()

    class Meta:
        verbose_name = 'Характеристика'
        verbose_name_plural = 'Характеристики'
        ordering = ('header', )

    def validate_unique(self, exclude=None):
        # Проверяем, что выбранный товар не имеет идиентичной характеристики.
        try:
            feature = ProductFeature.objects.get(product=self.product, header=self.header, name=self.name)
            if feature.id != self.id:
                raise ValidationError('Выбранный товар уже имеет идиентичную характеристику.')
        except models.ObjectDoesNotExist:
            pass

    def save(self, *args, **kwargs):
        # Сохраняем данные в нижнем регистре.
        self.header = self.header.capitalize()
        self.name = self.name.capitalize()

        self.validate_unique()
        super().save(*args, **kwargs)
    
    def __str__(self):
        return self.name
        

class CategoryFeature(models.Model):
    """Общие характеристики товаров одной категории"""
    category = models.ForeignKey(
        Category, 
        on_delete=models.CASCADE, 
        related_name='features',
        verbose_name='Категория',
        help_text='Присваивает текущую характеристику выбранной категории товаров')
    header = models.CharField(
        max_length=100, 
        verbose_name='Заголовок характеристики',
        help_text='Заголовок характеристики на странице с товаром')
    name = models.CharField(
        max_length=100, 
        verbose_name='Наименование')
    value = models.CharField(
        max_length=100, 
        verbose_name='Значение')
    description = models.CharField(
        max_length=100, 
        verbose_name='Описание', 
        help_text='Краткое описание характеристики', 
        blank=True)
    activate_feature = models.BooleanField(
        default=True, 
        verbose_name='Активировать характеристику',
        help_text='Если неактивно, cкрывает данную характеристику у всех товаров указанной категории.')
    activate_filter = models.BooleanField(
        default=True, 
        verbose_name='Активировать фильтр',
        help_text='Если неактивно, cкрывает фильтр по данной характеристике.')
    objects = ActiveManager()

    class Meta:
        verbose_name = 'Характеристики товаров одной категории'
        verbose_name_plural = 'Характеристики товаров одной категории'
        ordering = ('header', )

    def validate_unique(self, exclude=None):
        # Проверяем, что выбранная категория не имеет идиентичной характеристики.
        try:
            feature = CategoryFeature.objects.get(category=self.category, header=self.header, name=self.name)
            if feature.id != self.id:
                raise ValidationError('Выбранная категория уже имеет идиентичную характеристику.')
        except models.ObjectDoesNotExist:
            pass
    
    def create_product_feature(self):
        """Создаём идиентичную характеристику для всех продуктов указанной категории"""
        
        products = Product.objects.filter(categories__in=[self.category])
        
        for product in products:
            try:
                ProductFeature.objects.create(
                    product=product,
                    header=self.header,
                    name=self.name,
                    value=self.value
                )
            except ValidationError:
                pass
            
    def save(self, *args, **kwargs):
        # Сохраняем данные в нижнем регистре.
        self.header = self.header.capitalize()
        self.name = self.name.capitalize()
        self.value = self.value.capitalize()

        self.validate_unique()
        self.create_product_feature()
        super().save(*args, **kwargs)
    
    def __str__(self):
        return self.name


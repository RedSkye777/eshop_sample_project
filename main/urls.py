from django.urls import path

from . import views

urlpatterns = [
    path(
        '500/', 
        views.error500, 
        name = '500'
    ),
    path(
        '404/', 
        views.error404, 
        name = '404'
    ),
    path(
        'search/', 
        views.search, 
        name = 'search'
    ),
    path(
        'product-list-by-filter/', 
        views.product_list_by_filter, 
        name = 'product_list_by_filter'
    ),
    path(
        'ajax/get-filter-data/', 
        views.getFilterData, 
        name='get_filter_data'
    ),
    path(
        'set-user-city/',
        views.save_user_city_in_session,
        name='set_user_city'
    ),
    path(
        'products/',
        views.product_list_view,
        name='product_list'
    ),
    path(
        'products/<slug:category_slug>/',
        views.product_list_view,
        name='product_list_by_category'
    ),
    path(
        'product/<uuid:pk>/<slug:slug>/', 
        views.ProductDetailView.as_view(),
        name='product_detail'
    ),
    path(
        'otzywy/',
        views.RepliesPageView.as_view(),
        name='replies'
    ),
    path(
        'shipping/',
        views.ShippingPageView.as_view(),
        name='shipping'
    ),
    path(
        'contact-us/',
        views.ContactUsPageView.as_view(),
        name='contact_us'
    ),
    path(
        'about-us/',
        views.AboutUsPageView.as_view(),
        name='about_us'
    ),
    path(
        '',
        views.home_page_view,
        name='home'
    ),
]

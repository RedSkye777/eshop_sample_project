from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from main.models import Product, Category

@receiver(m2m_changed, sender=Product.categories.through)
def LinkCategoryAncestors(sender, instance, action, pk_set, **kwargs):
    """Linking product to ancestors of just added category"""

    if action == 'post_add':
        ancestor_list = []
        pk_list = list(pk_set)
        added_categories = Category.objects.filter(pk__in=pk_list)

        for category in added_categories:
            for ancestor in category.get_ancestors():
                if ancestor not in ancestor_list:
                    ancestor_list.append(ancestor)
        
        instance.categories.add(*ancestor_list)



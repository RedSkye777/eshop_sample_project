from import_export import resources
from import_export.admin import ImportExportMixin
from imagekit.admin import AdminThumbnail
from mptt.admin import MPTTModelAdmin

from django.contrib import admin

from main import models, forms

class ProductResources(resources.ModelResource):

    class Meta:
        model = models.Product
        fields = ('id', 'name', 'price', 'number_in_stock', 'number_reserved', 'number_purchased', 'weight', 'length', 'height', 'volume', 'description')

class ProductsInline(admin.StackedInline):
    model = models.Category.products.through
    extra = 0

class ProductExtraImagesInline(admin.TabularInline):
    model = models.ProductExtraImage
    extra = 1

class ProductsFeaturesInline(admin.TabularInline):
    model = models.ProductFeature
    extra = 0
    form = forms.ProductFeatureCreationForm
    can_delete = False

class CategoryFeaturesInline(admin.TabularInline):
    model = models.CategoryFeature
    extra = 0
    form = forms.CategoryFeatureCreationForm
    can_delete = False

@admin.register(models.ProductFeature)
class ProductFeatureAdmin(admin.ModelAdmin):
    list_display = ['product', 'header', 'name', 'value', 'active']
    fields = (
        'product', 'header', 'name', 'value', 'active'
    )
    form = forms.ProductFeatureCreationForm

@admin.register(models.CategoryFeature)
class CategoryFeatureAdmin(admin.ModelAdmin):
    list_display = ['category', 'header', 'name', 'value', 'activate_feature', 'activate_filter']
    fields = (
        'category', 'header', 'name', 'value', 'activate_feature', 'activate_filter'
    )
    form = forms.CategoryFeatureCreationForm

@admin.register(models.Category)
class CategoryAdmin(MPTTModelAdmin):
    fields = (
        'parent', 'name', 'description', 'active'
    )
    search_fields = ('name', 'description')
    inlines = [CategoryFeaturesInline, ProductsInline]

@admin.register(models.ProductBrand)
class ProductBrandAdmin(admin.ModelAdmin):
    fields = (
        'name',
    )

# @admin.register(models.ProductExtraImage)
# class ProducExtraImageAdmin(admin.ModelAdmin):
#     fields = (
#         'product', 'image', 'admin_thumbnail'
#     )
#     readonly_fields = ('admin_thumbnail',)
#     admin_thumbnail = AdminThumbnail(image_field='thumbnail', template='admin/thumbnail.html')

@admin.register(models.Product)
class ProductAdmin(ImportExportMixin, admin.ModelAdmin):
    fieldsets = (
        ('Основное', {
            'fields': ('name', 'description', 'image', 'categories')
        }),
        ('Данные', {
            'fields': ('price', 'number_in_stock', 'number_reserved', 'number_purchased', 'weight', 'length', 'height', 'volume', 'active')
        }),
        ('SEO', {
            'fields': ('slug', )
        }),
    )
    autocomplete_fields = ('categories', )
    search_fields = ('name', 'description')
    resource_class = ProductResources
    inlines = [ProductExtraImagesInline, ProductsFeaturesInline]

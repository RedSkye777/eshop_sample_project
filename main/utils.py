import logging
import requests
import time
import functools

from django.conf import settings
from django.db import connection, reset_queries
from django.http import HttpResponseBadRequest

from main.models import Product, ProductFeature, Category

logger = logging.getLogger(__name__)
logger.setLevel('DEBUG')

formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler('logs/utils.log')
file_handler.setLevel('DEBUG')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

def query_debugger(func):

    @functools.wraps(func)
    def inner_func(*args, **kwargs):

        reset_queries()
        
        start_queries = len(connection.queries)

        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()

        end_queries = len(connection.queries)

        logger.debug(f"Function : {func.__name__}")
        logger.debug(f"Number of Queries : {end_queries - start_queries}")
        logger.debug(f"Finished in : {(end - start):.2f}s")
        return result

    return inner_func

def check_recaptcha(function):
    def wrap(request, *args, **kwargs):
        request.recaptcha_is_valid = None
        if request.method == 'POST':
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.RECAPTCHA_PRIVATE_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            if result['success']:
                request.recaptcha_is_valid = True
            else:
                request.recaptcha_is_valid = False
        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def ajax_required(f):
    def wrap(request, *args, **kwargs):
        if request.method == 'POST' and not request.is_ajax():
            return HttpResponseBadRequest()
        return f(request, *args, **kwargs)
    wrap.__doc__=f.__doc__
    wrap.__name__=f.__name__
    return wrap

def get_form_errors(form):
    """Return all form errors as list"""
    error_list = []
    if form.errors:
        for field in form:
            for error in field.errors:
                error_list.append(error)
        for error in form.non_field_errors():
            error_list.append(error)
    return error_list

    
from datetime import timedelta

from celery.task import periodic_task

from project_settings.celery import app

@app.task
def test_task():
    print('Hi there')

@periodic_task(run_every=(timedelta(seconds=10)))
def test_print():
    print('Hi there')



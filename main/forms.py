import logging

from django import forms
from django.db import models
from django.core.mail import send_mail
from django.conf import settings

from main.models import ProductFeature, CategoryFeature

logger = logging.getLogger(__name__)

class ContactForm(forms.Form):
    email= forms.EmailField(required=False)
    message = forms.CharField(required=False)

    error_messages = {
        'email_missed': 'Вы не указали email для обратной связи.',
        'message_missed': 'Вы ничего не написали в своём сообщении.'
    }

    def clean(self):
        email = self.cleaned_data.get('email', None)
        message = self.cleaned_data.get('message', None)

        if not email:
            raise forms.ValidationError(
                self.error_messages['email_missed'],
                code='email_missed'
            )

        if not message:
            raise forms.ValidationError(
                self.error_messages['message_missed'],
                code='message_missed'
            )

    def send_mail(self):
        logger.info("Sending email to customer service")
        message = "От: {0}\n{1}".format(
            self.cleaned_data["email"],
            self.cleaned_data["message"],
        )
        send_mail(
            "Кто-то захотел связаться с Вами.",
            message,
            settings.PROJECT_EMAIL_ADDRESS,
            [settings.PROJECT_CUSTOMER_SERVICE_EMAIL_ADDRESS],
            fail_silently=False,
        )

class SetUserCityForm(forms.Form):
    status = forms.CharField(required=False)
    city = forms.CharField(required=False)

    error_messages = {
        'status_missed': 'Status is not found.',
        'city_missed': 'User city is not found.',
        'already_set': 'City has been already set.'
    }

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    def clean(self):
        status = self.cleaned_data.get('status')
        city = self.cleaned_data.get('city')

        if not status:
            raise forms.ValidationError(
                self.error_messages['status_missed'],
                code='status_missed',
            )
        if not city:
            raise forms.ValidationError(
                self.error_messages['city_missed'],
                code='city_missed',
            )
        if self.request:
            current_city = self.request.session.get(settings.CITY_SESSION_ID)
            if current_city:
                raise forms.ValidationError(
                    self.error_messages['already_set'],
                    code='already_set',
                )

    def set_new_city(self):
        session = self.request.session
        city = self.cleaned_data.get('city')
        
        session[settings.CITY_SESSION_ID] = city

class ProductFeatureCreationForm(forms.ModelForm):
    class Meta:
        model = ProductFeature
        fields = ['product', 'header', 'name', 'value', 'active']
    
    error_messages = {
        'feature_already_exists': 'Выбранный товар уже имеет идиентичную характеристику',
        'product_field_not_found': {
            'product': ['Обязательное поле']
        },
        'header_field_not_found': {
            'header': ['Обязательное поле']
        },
        'name_field_not_found': {
            'name': ['Обязательное поле']
        },
    }

    def clean(self):
        product = self.cleaned_data.get('product')
        header = self.cleaned_data.get('header')
        name = self.cleaned_data.get('name')

        if not product:
            raise forms.ValidationError(
                self.error_messages['product_field_not_found'],
                code='product_field_not_found',
            )
        if not header:
            raise forms.ValidationError(
                self.error_messages['header_field_not_found'],
                code='header_field_not_found',
            )
        if not name:
            raise forms.ValidationError(
                self.error_messages['name_field_not_found'],
                code='name_field_not_found',
            )
        # Проверяем, что выбранный товар не имеет идиентичной характеристики.
        try:
            feature = ProductFeature.objects.get(product=product, header=header.capitalize(), name=name.capitalize())
            if feature.id != self.instance.id:
                raise forms.ValidationError(
                    self.error_messages['feature_already_exists'],
                    code='feature_already_exists',
                )
        except models.ObjectDoesNotExist:
            pass

class CategoryFeatureCreationForm(forms.ModelForm):
    class Meta:
        model = CategoryFeature
        fields = ['category', 'header', 'name', 'value', 'activate_feature', 'activate_filter']
    
    error_messages = {
        'feature_already_exists': 'Выбранная категория уже имеет идиентичную характеристику',
        'category_field_not_found': {
            'category': ['Обязательное поле']
        },
        'header_field_not_found': {
            'header': ['Обязательное поле']
        },
        'name_field_not_found': {
            'name': ['Обязательное поле']
        },
    }

    def clean(self):
        category = self.cleaned_data.get('category')
        header = self.cleaned_data.get('header')
        name = self.cleaned_data.get('name')

        if not category:
            raise forms.ValidationError(
                self.error_messages['category_field_not_found'],
                code='category_field_not_found',
            )
        if not header:
            raise forms.ValidationError(
                self.error_messages['header_field_not_found'],
                code='header_field_not_found',
            )
        if not name:
            raise forms.ValidationError(
                self.error_messages['name_field_not_found'],
                code='name_field_not_found',
            )
        
        # Проверяем, что выбранныая категория не имеет идиентичной характеристики.
        try:
            feature = CategoryFeature.objects.get(category=category, header=header.capitalize(), name=name.capitalize())
            if feature.id != self.instance.id:
                raise forms.ValidationError(
                    self.error_messages['feature_already_exists'],
                    code='feature_already_exists',
                )
        except models.ObjectDoesNotExist:
            pass

import json

from django.db.models import Count, ObjectDoesNotExist, Q, F, Prefetch, Count
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.base import TemplateView
from django.views.generic import FormView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.http import Http404, JsonResponse, HttpResponseBadRequest
from django.urls import reverse_lazy, reverse
from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.cache import cache
from django.views.decorators.cache import cache_page

from main import models, forms
from main.utils import ajax_required
from page.models import AboutUsPage, HomePage, ContactUsPage, PaymentPage, RepliesPage, ShippingPage


@cache_page(60)
def home_page_view(request):

    return redirect(reverse('product_list'))

    if request.method == 'GET':
        template_name = 'main/home.html'
        response_dict = {}

        qs = HomePage.objects.all()

        if qs.exists():
            page = qs.first()
        else:
            page = HomePage.objects.create(name='Домашняя страница', title='Домашняя страница')
        
        queryset = models.Product.objects.only('name', 'image', 'price')
        
        response_dict['page'] = page
        response_dict['hot_product_list'] = queryset.order_by('-number_purchased')[:4]
        response_dict['new_product_list'] = queryset.order_by('-date_updated')[:4]

        return render(request, template_name, response_dict)

    return HttpResponseBadRequest


class AboutUsPageView(TemplateView):
    template_name = 'main/about_us.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        qs = AboutUsPage.objects.all()

        if qs.exists():
            page = qs.first()
        else:
            page = AboutUsPage.objects.create(name='Информация о компании', title='Информация о компании')
        
        context['page'] = page
        
        return context


class ContactUsPageView(TemplateView):
    template_name = 'main/contact_us.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        qs = ContactUsPage.objects.all()

        if qs.exists():
            page = qs.first()
        else:
            page = ContactUsPage.objects.create(name='Контакты', title='Контакты')
        
        context['page'] = page
        
        return context


class ShippingPageView(TemplateView):
    template_name = 'main/shipping.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        qs = ShippingPage.objects.all()

        if qs.exists():
            page = qs.first()
        else:
            page = ShippingPage.objects.create(name='Информация о доставке', title='Информация о доставке')
        
        context['page'] = page

        return context
    

class RepliesPageView(TemplateView):
    template_name = 'main/replies.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        qs = RepliesPage.objects.all()

        if qs.exists():
            page = qs.first()
        else:
            page = RepliesPage.objects.create(name='Отзывы', title='Отзывы')
        
        context['page'] = page

        return context


class ProductDetailView(DetailView):
    model = models.Product
    template_name = 'main/product_detail.html'


@ajax_required
def save_user_city_in_session(request):
    """
    Get ajax request from js script and check whether user city is saved in django session.
    If not, save user city in django session
    """

    form = forms.SetUserCityForm(data=request.POST, request=request)

    if form.is_valid():
        form.set_new_city()
        return JsonResponse({'status': 'ok'})
    else:
        error = form.non_field_errors()[0]
        return JsonResponse({'status': 'ko', 'error': error}, safe=False)


def getFilterData(request):
    """Принимаем ключ для фильтрации и отправляем результат"""
    if request.method == 'GET' and request.is_ajax():
        filter_key = request.GET.get('filter_key')
        response = models.ProductFeature.objects.filter(name__iexact=filter_key).values_list('value').order_by('value').annotate(quantity=Count('value'))
        response = [(i[0], i[1]) for i in list(response)]
        data = {
            "response": response, 
        }
        return JsonResponse(data, status = 200)


@ajax_required
def search(request):
    if request.method == 'GET':
        results = []
        queryList = models.Product.objects.active()
        query = request.GET.get('query')

        if query:
            if len(query) > 0:
                queryList = queryList.filter(
                    Q(name__istartswith=query) | Q(categories__name__istartswith=query)
                ).prefetch_related(Prefetch('categories')).distinct()[:5]
        else:
            queryList = []
        
        for product in queryList:
            category_list = []

            for category in product.get_category_tree():
                category_list.append(category.name.capitalize())

            if product.image:
                thumbnail_url = product.image.url
            else:
                thumbnail_url = '/static/images/image-not-found.png'

            results.append({
                'id':product.id,
                'name':product.name,
                'categories': category_list,
                'absolute_url': product.get_absolute_url(),
                'thumbnail_url': thumbnail_url,
            })

        count = len(queryList)

        return JsonResponse({'results': results, 'count': count}, status=200)

def handler404(request, exception):
    return render(request, 'errors/404.html')

def handler500(request):
    return render(request, 'errors/500.html')

def error404(request):
    return render(request, 'errors/404.html')

def error500(request):
    return render(request, 'errors/500.html')

@cache_page(600)
def product_list_view(request, category_slug=None):
    template_name = 'main/product_list.html'

    if request.method == 'GET':
        page = request.GET.get('page')

        feature_queryset = models.ProductFeature.objects.filter(active=True)

        product_queryset = models.Product.objects.filter(active=True)           \
            .prefetch_related(Prefetch('features', queryset=feature_queryset))  \
            .order_by('-number_purchased')
        
        if category_slug:
            category = get_object_or_404(models.Category, active=True, slug=category_slug)
            product_queryset = product_queryset.filter(categories__slug=category_slug)

            category_children = category.get_children()
            category_ancestors = category.get_ancestors()
            category_features = category.features.filter(activate_filter=True)

        else:
            category = None
            category_children = None
            category_ancestors = None
            category_features = None

        paginator_obj = Paginator(product_queryset, 6)
        try:
            product_list = paginator_obj.page(page)
        except PageNotAnInteger:
            product_list = paginator_obj.page(1)
        except EmptyPage:
            product_list = []
        
        response_dict = {
            'product_list': product_list, 
            'category': category, 
            'category_children': category_children,
            'category_ancestors': category_ancestors,
            'category_features': category_features
        }

        return render(request, template_name, response_dict)

    else:
        return HttpResponseBadRequest()


@ajax_required
def product_list_by_filter(request):
    SORTING_OPTIONS = {
        'popularity': '-number_purchased',
        'price_increase': 'price',
        'price_decrease': '-price',
    }

    results = []

    if request.method == 'GET':
        
        queryset  = models.Product.objects.filter(active=True)
        category = request.GET.get('category')
        sort_by = request.GET.get('sort_by')
        filter_list = request.GET.get('ind_filter_list')
        price_from = request.GET.get('price_from')
        price_to = request.GET.get('price_to')
        page = request.GET.get('page')

        if category:
            queryset = queryset.filter(categories__name__in=[category])
        
        if sort_by and sort_by in SORTING_OPTIONS:
            queryset = queryset.order_by(SORTING_OPTIONS[sort_by])
                
        if filter_list:
            filter_list = json.loads(filter_list)
            value_list = filter_list.values()

            for key in filter_list.keys():
                queryset = queryset.filter(features__name__iexact=key, features__value__in=filter_list[key])
        
        if price_from:
            price_from = json.loads(price_from)
            queryset = queryset.filter(price__gte=price_from)
        
        if price_to:
            price_to = json.loads(price_to)
            queryset = queryset.filter(price__lte=price_to)

        queryset = queryset.prefetch_related(Prefetch('features')) \
            .only('name', 'price', 'image').distinct() \
            
        current_page = Paginator(queryset, 6)
        
        product_list = []

        try:
            product_list = current_page.page(page).object_list
        except PageNotAnInteger:
            product_list = []  
        except EmptyPage:
            product_list = []

        for product in product_list:
            feature_list = []

            for feature in product.features.all():
                feature_list.append({'name': feature.name, 'value': feature.value})
            
            if product.image:
                thumbnail_url = product.image.url
            else:
                thumbnail_url = '/static/images/image-not-found.png'

            results.append({
                'id':product.id,
                'name':product.name,
                'price':product.price,
                'thumbnail_url': thumbnail_url,
                'absolute_url': product.get_absolute_url(),
                'features': feature_list
            })

    return JsonResponse({'results': results}, status=200)

from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'main'

    verbose_name = 'Основное'
    verbose_name_plural = 'Основное'

    def ready(self):
        import main.signals
        

import csv
import datetime

from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.http import HttpResponse

from order.models import Order, OrderItem
from payment.views import payment_refund

def order_detail(obj):
    return	mark_safe('<a href="{}">View</a>'.format(								
        reverse('order:admin_order_detail', args=[obj.id])))

def make_refund(obj):
    return	mark_safe('<a href="{}">Make refund</a>'.format(								
        reverse('order:admin_order_detail', args=[obj.id])))

def export_to_csv(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition']	= 'attachment;'\
        'filename={}.csv'.format(opts.verbose_name)		
    writer = csv.writer(response)

    fields = [field for field in opts.get_fields() if not field.many_to_many and not field.one_to_many]	

    # Write	a first	row	with header	information				
    writer.writerow([field.verbose_name	for	field in fields])
    # Write data rows
    for obj in queryset:
        data_row = []
        for field in fields:
            value = getattr(obj, field.name)
            if isinstance(value, datetime.datetime):
                value =	value.strftime('%d/%m/%Y')
            data_row.append(value)
        writer.writerow(data_row)
    return	response
export_to_csv.short_description	= 'Экспортировать в CSV формате'

class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    readonly_fields = ('product', 'price', 'quantity')

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'delivery_address', 'payment_status', 'created']    
    list_filter = ['payment_status', 'created', 'updated']
    fieldsets = (
        ('Информация о покупателе', {
            'fields': ('name', 'phone_number', 'email')
        }),
        ('Информация о доставке', {
            'fields': ('delivery_method', 'delivery_detail', 'delivery_address', 'delivery_tariff', 'delivery_id', 'delivery_comment'),
        }),
        ('Информация об оплате', {
            'fields': ('payment_status', 'payment_method', 'payment_expiration_period', 'payment_id')
        })
    )
    inlines = [OrderItemInline, ]
    actions = [export_to_csv]
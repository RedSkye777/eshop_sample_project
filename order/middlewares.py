from django.conf import settings
from django.db.models import ObjectDoesNotExist

from order.models import Order

# Проверяем, есть ли в сессии неоплаченный заказ
def order_middleware(get_response):
    def middleware(request):
        if not settings.ORDER_SESSION_ID in request.session:
            request.order = None
        else:
            order_id = request.session[settings.ORDER_SESSION_ID]
            acceptable_statuses = ['pending',]

            try:
                order = Order.objects.get(id=order_id, payment_status__in=acceptable_statuses)
                request.order = order
            except ObjectDoesNotExist:
                request.order = None

        response = get_response(request)
        return response
    return middleware
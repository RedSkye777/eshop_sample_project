from django.urls import path

from order import views

app_name = 'order'

urlpatterns = [
    path(
        '<int:order_id>/canceled/',
        views.order_canceled,
        name='canceled'
    ),
    path(
        '<int:order_id>/done/',
        views.order_done,
        name='done'
    ),
    path(
        'new-order/',
        views.new_order,
        name='new_order'
    ),
    path(
        'admin/order/<int:order_id>/',
        views.admin_order_detail,
        name='admin_order_detail'
    ),
]

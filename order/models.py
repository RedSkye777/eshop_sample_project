from django.db.models import Sum, FloatField, F
from django.db import models

from main.models import Product
from main.utils import query_debugger
from eshop_sample_project.users.models import User


class Order(models.Model):
    DELIVERY_METHOD_OPTIONS = [
        ('cdek', 'Доставка транспортной-компанией СДЭК'),
        ('pickup', 'Самовывоз из магазина'),
    ]
    DELIVERY_DETAIL_OPTIONS = [
        ('to_apartment', 'Доставка до квартиры'),
        ('to_pickup_point', 'Доставка до пункта самовывоза')
    ]
    PAYMENT_STATUS_OPTIONS = [
        ('pending', 'В ожидании оплаты'),
        ('waiting_for_capture', 'Средства удержаны'),
        ('succeeded', 'Оплачен'),
        ('canceled', 'Отменён'),
        ('refund', 'Средства возвращены'),
    ]
    PAYMENT_METHOD_OPTIONS = [
        ('online', 'Через платежный шлюз'),
        ('in_shop', 'При самовывозе из магазина'),
    ]
    user = models.ForeignKey(
        User,
        related_name='Orders',
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name='Пользователь'
    )
    name = models.CharField(
        max_length=150, 
        null=True, 
        blank=True,
        verbose_name='Имя'
    )
    email = models.EmailField(
        verbose_name='Электронная почта'
    )
    phone_number = models.CharField(
        max_length=50,
        verbose_name='Номер телефона'
    )
    delivery_method = models.CharField(
        max_length=50,
        choices=DELIVERY_METHOD_OPTIONS,
        verbose_name='Способ доставки'
    )
    delivery_detail = models.CharField(
        max_length=50,
        choices=DELIVERY_DETAIL_OPTIONS,
        verbose_name='Доставка до',
        blank=True,
        null=True
    )
    delivery_address = models.CharField(
        max_length=200,
        verbose_name='Адрес доставки'
    )
    delivery_tariff = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name='Тариф доставки'
    )
    delivery_id = models.CharField(
        max_length=150, 
        null=True, 
        blank=True,
        verbose_name='ID доставки', 
    )
    delivery_comment = models.TextField(
        null=True, 
        blank=True,
        verbose_name='Комментарий к доставке'
    )
    payment_status = models.CharField(
        max_length=50,
        choices=PAYMENT_STATUS_OPTIONS,
        default='pending',
        verbose_name='Статус оплаты'
    )
    payment_method = models.CharField(
        max_length=50,
        choices=PAYMENT_METHOD_OPTIONS,
        null=True, 
        blank=True,
        verbose_name='Способ оплаты'
    )
    payment_expiration_period = models.PositiveIntegerField(
        default=24,
        verbose_name='Допустимое время на оплату', 
        help_text='Время (в часах), отведённое пользователю на оплату. Если по истечении срока заказ не будет оплачен, то он перейдёт в статус "Отменён".'
    )
    payment_id = models.CharField(
        max_length=150, 
        null=True, 
        blank=True,
        verbose_name='ID платежа', 
        help_text='ID платежа в соответствующей платёжной системе'
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ('-created', )
    
    def __str__(self):
        return 'Order {}'.format(self.id)
    
    def get_total_cost(self):
        try:
            total_cost = sum(item.get_cost() for item in self.items.all())
            return total_cost
        except AttributeError:
            return 'Связанные товары не обнаружены.'

class OrderItem(models.Model):
    order = models.ForeignKey(
        Order,
        related_name='items',
        on_delete=models.CASCADE
    )
    product = models.ForeignKey(
        Product,
        related_name='order_items',
        on_delete=models.DO_NOTHING,
        verbose_name='Товар'
    )
    quantity = models.PositiveIntegerField(
        default=1,
        verbose_name='Колличество'
    )
    price = models.DecimalField(
        max_digits=12, decimal_places=2,
        verbose_name='Цена'
    )

    class Meta:
        verbose_name = 'Запрошенный товар'
        verbose_name_plural = 'Запрошенные товары'
        ordering = ('-price', )

    def __str__(self):
        return '{}'.format(self.id)
    
    def get_cost(self):
        return self.price * self.quantity

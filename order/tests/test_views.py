from django.test import TestCase
from django.urls import reverse
from django.conf import settings

from order import views, forms
from main.models import Product

class TestOrderViews(TestCase):

    def test_create_order_view(self):

        # test redirecting to cart page if user has no cart or cart is empty
        response = self.client.get(reverse('order:new_order'), follow=True)

        self.assertRedirects(
            response,
            reverse('cart:cart_detail')
        )
        
        self.assertTemplateUsed(
            response,
            'cart/cart.html'
        )

        # test redirecting to new_order.html if user has cart
        product = Product.objects.create(
            name='Apple',
            price=123,
            number_in_stock=34
        )

        ajax_data = {'product_id': str(product.id), 'quantity': '34'}
        response = self.client.post(reverse('cart:add_to_cart'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ok'
        )

        response = self.client.get(reverse('order:new_order'))

        self.assertEqual(
            response.status_code,
            200
        )
        
        self.assertTemplateUsed(
            response,
            'order/new_order.html'
        )
        self.assertIsInstance(
            response.context['form'],
            forms.OrderCreationForm
        )

        # test ajax_required decorator required if request.method == 'POST'
        response = self.client.post(reverse('order:new_order'), follow=True)

        self.assertEqual(
            response.status_code,
            400
        )

        # Test getting json errors if form is not valid

        incorrect_order_data = {
        }

        response = self.client.post(reverse('order:new_order'), data=incorrect_order_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ko'
        )

        error_list = response.json()['errors']

        self.assertEqual(
            len(error_list),
            1
        )

        # Test successfull order creation
        
        correct_order_data = {
            'phone': '777888444555',
            'name': 'Max',
            'email': 'test@gmail.com',
            'delivery_method': 'delivery',
            'postal_code': '414515412',
            'address': 'moscow house 32',
            'payment_method': 'online',
        }

        response = self.client.post(reverse('order:new_order'), data=correct_order_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ok'
        )

        self.assertEqual(
            response.json()['redirect_to'],
            reverse('payment:process')
        )
        


        

from django.test import TestCase
from django.urls import reverse
from django.conf import settings

from order import forms
from order.models import Order, OrderItem
from main.models import Product
from cart.cart import Cart
from account.models import User

class TestOrderForms(TestCase):

    def setUp(self):
        self.request = self.client.get(reverse('home')).wsgi_request

    def test_order_creation_form_errors(self):

        error_cases = {
            'phone_missed': {
                '': ''
            },
            'email_missed': {
                'phone': '777888444555',
            },
            'name_missed': {
                'phone': '777888444555',
                'email': 'test@gmail.com'
            },
            'delivery_method_missed': {
                'phone': '777888444555',
                'name': 'Max',
                'email': 'test@gmail.com'
            },
            'incorrect_delivery_method': {
                'phone': '777888444555',
                'name': 'Max',
                'email': 'test@gmail.com',
                'delivery_method': 'Some new delivery method'
            },
            'postal_code_missed': {
                'phone': '777888444555',
                'name': 'Max',
                'email': 'test@gmail.com',
                'delivery_method': 'delivery'
            },
            'address_missed': {
                'phone': '777888444555',
                'name': 'Max',
                'email': 'test@gmail.com',
                'delivery_method': 'delivery',
                'postal_code': '414515412'
            },
            'payment_method_missed': {
                'phone': '777888444555',
                'name': 'Max',
                'email': 'test@gmail.com',
                'delivery_method': 'delivery',
                'postal_code': '414515412',
                'address': 'moscow house 32'
            },
            'incorrect_payment_method': {
                'phone': '777888444555',
                'name': 'Max',
                'email': 'test@gmail.com',
                'delivery_method': 'delivery',
                'postal_code': '414515412',
                'address': 'moscow house 32',
                'payment_method': 'loan'
            },
        }

        # Test cart_is_not_determined error 

        error_code = 'cart_is_not_determined'
        form = forms.OrderCreationForm(data={}, request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test cart_is_empty error 

        self.request.session[settings.CART_SESSION_ID] = {}

        error_code = 'cart_is_empty'
        form = forms.OrderCreationForm(data={}, request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test product_number_in_stock_changed error 

        cart = self.request.session[settings.CART_SESSION_ID]

        product = Product.objects.create(
            name='Can',
            price=23,
            number_in_stock=250
        )

        cart[str(product.id)] = {'quantity': 250}

        product.number_in_stock = 200
        product.save()

        error_code = 'product_number_in_stock_changed'
        form = forms.OrderCreationForm(data={}, request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )
        cart[str(product.id)] = {'quantity': 200}

        # Test phone_missed error 
        error_code = 'phone_missed'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test email_missed error 
        error_code = 'email_missed'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test name_missed error 
        error_code = 'name_missed'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test delivery_method error 
        error_code = 'delivery_method_missed'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test incorrect_delivery_method error 
        error_code = 'incorrect_delivery_method'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test postal_code_missed error 
        error_code = 'postal_code_missed'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test address_missed error 
        error_code = 'address_missed'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test payment_method_missed error 
        error_code = 'payment_method_missed'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

        # Test incorrect_payment_method error 
        error_code = 'incorrect_payment_method'
        form = forms.OrderCreationForm(data=error_cases[error_code], request=self.request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            len(form.errors),
            1
        )
        self.assertEqual(
            form.non_field_errors().as_data()[0].code,
            error_code
        )

    def test_create_order_method(self):
        
        # Создаём подготовительные данные
        apple = Product.objects.create(
            name='Apple',
            price=10,
            number_in_stock=200
        )

        carrot = Product.objects.create(
            name='Carrot',
            price=5,
            number_in_stock=200
        )

        # Наполняем стартовую корзину
        cart = self.request.session[settings.CART_SESSION_ID] = {}
        cart[str(apple.id)] = {'price': str(apple.price), 'quantity': 100}
        cart[str(carrot.id)] = {'price': str(carrot.price), 'quantity': 100}

        # Передаём форме корректные данные о заказе
        correct_order_data = {
            'phone': '777888444555',
            'name': 'Max',
            'email': 'test@gmail.com',
            'delivery_method': 'delivery',
            'postal_code': '414515412',
            'address': 'moscow house 32',
            'payment_method': 'online',
        }

        form = forms.OrderCreationForm(data=correct_order_data, request=self.request)

        self.assertTrue(
            form.is_valid()
        )

        form.create_order()

        # Проверяем, что заказ был успешно создан и в нём два товара.
        self.assertEqual(
            Order.objects.all().count(),
            1
        )

        order = Order.objects.get(id=1)

        self.assertEqual(
            len(order.items.all()),
            2
        )

        # Проверяем, что итоговая сумма заказа верна.
        self.assertEqual(
            order.get_total_cost(),
            1500
        )

        # Проверяем, что колличество товара в запасе изменилось.
        product_slugs = ['apple', 'carrot']

        for slug in product_slugs:
            self.assertEqual(
                Product.objects.get(slug=slug).number_in_stock,
                100
            )

            self.assertEqual(
                Product.objects.get(slug=slug).number_reserved,
                100
            )

        # Проверяем, что корзина была удалена из сессии
        self.assertTrue(
            settings.CART_SESSION_ID not in self.request.session
        )



        
from django.test import TestCase

from order import models
from main.models import Product

class TestOrderModels(TestCase):

    def test_order_model_methods(self):

        product_1 = Product.objects.create(
            name='Scissors',
            price=300,
            number_in_stock=124
        )
        product_2 = Product.objects.create(
            name='Book',
            price=1000,
            number_in_stock=200
        )

        order = models.Order.objects.create(
            email='test@gmail.com',
            postal_code='412415515',
            address='Moscow'
        )

        orderItem_1 = models.OrderItem.objects.create(
            order=order,
            product=product_1,
            price=product_1.price,
            quantity=5
        )

        orderItem_2= models.OrderItem.objects.create(
            order=order,
            product=product_2,
            price=product_2.price,
            quantity=10
        )

        self.assertEqual(
            orderItem_1.get_cost(),
            1500
        )

        self.assertEqual(
            order.get_total_cost(),
            11500
        )

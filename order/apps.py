from django.apps import AppConfig


class OrderConfig(AppConfig):
    name = 'order'

    verbose_name = 'Заказ'
    verbose_name_plural = 'Заказы'

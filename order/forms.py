from django import forms
from django.conf import settings
from django.db.models import ObjectDoesNotExist

from order.models import Order, OrderItem
from main.models import Product
from cart.cart import Cart

class OrderCreationForm(forms.Form):
    name = forms.CharField(required=False, max_length=150)
    email = forms.EmailField(required=False)
    phone_number = forms.CharField(required=False, max_length=20)
    
    delivery_method = forms.CharField(required=False, max_length=50)
    delivery_detail = forms.CharField(required=False, max_length=50)
    delivery_address = forms.CharField(required=False, max_length=200)
    delivery_tariff = forms.CharField(required=False, max_length=100)
    delivery_comment = forms.CharField(required=False)

    payment_method = forms.CharField(required=False, max_length=50)

    error_messages = {
        'name_missed': '"name" is not found.',
        'email_missed': '"email" is not found.',
        'phone_number_missed': '"phone_number" is not found.',

        'delivery_method_missed': '"delivery_method" is not found.',
        'delivery_detail_missed': '"delivery_detail" is required for this delivery method.',
        'delivery_address_missed': '"delivery_address" is not found.',
        'payment_method_missed': '"payment_method" is not found.',

        'incorrect_delivery_method': 'This delivery method does not exist or not available.',
        'incorrect_delivery_detail_option': 'This delivery detail option does not exist or not available',
        'incorrect_payment_method': 'This payment method does not exist or not available.',

        'cart_is_not_determined': 'Cart is not determined in the user session.',
        'cart_is_empty': 'Cart is empty.',
    }

    delivery_methods = [
        'cdek',
        'pickup'
    ]

    delivery_detail_options = [
        'to_apartment',
        'to_pickup_point'
    ]

    payment_methods = [
        'online',
        'in_shop'
    ]

    def __init__(self, request, *args, **kwargs):
        self.request = request
        self.session = request.session
        self.user = None
        if request.user.is_authenticated:
            self.user = request.user
        super().__init__(*args, **kwargs)

    def clean(self):
        name = self.cleaned_data.get('name')
        email = self.cleaned_data.get('email')
        phone_number = self.cleaned_data.get('phone_number')
        
        delivery_method = self.cleaned_data.get('delivery_method')
        delivery_detail = self.cleaned_data.get('delivery_detail')
        delivery_address = self.cleaned_data.get('delivery_address')

        payment_method = self.cleaned_data.get('payment_method')

        # Check if cart exists in user session
        if not settings.CART_SESSION_ID in self.session:
            raise forms.ValidationError(
                self.error_messages['cart_is_not_determined'],
                code='cart_is_not_determined'
            )
        else:
            # Check if cart is empty
            cart = self.session[settings.CART_SESSION_ID]
            if not cart:
                raise forms.ValidationError(
                    self.error_messages['cart_is_empty'],
                    code='cart_is_empty'
                )
            # Проверяем, что колличество товара, запрошенное в корзине, всё ещё доступно. 
            for product_id in cart.keys():
                product_quantity = cart[product_id]['quantity']
                product_number_in_stock = Product.objects.get(id=product_id).number_in_stock
                if product_quantity > product_number_in_stock:
                    raise forms.ValidationError(
                        'Unfortunately, max quantity of product has been changed. Only {} is left'.format(product_number_in_stock),
                        code='product_number_in_stock_changed'
                    )
        if not phone_number:
            raise forms.ValidationError(
                self.error_messages['phone_number_missed'],
                code='phone_number_missed'
            )
        if not email:
            raise forms.ValidationError(
                self.error_messages['email_missed'],
                code='email_missed'
            )
        if not name:
            raise forms.ValidationError(
                self.error_messages['name_missed'],
                code='name_missed'
            )
        if not delivery_method:
            raise forms.ValidationError(
                self.error_messages['delivery_method_missed'],
                code='delivery_method_missed'
            )
        else:
            if delivery_method not in self.delivery_methods:
                raise forms.ValidationError(
                    self.error_messages['incorrect_delivery_method'],
                    code='incorrect_delivery_method'
                )
        if delivery_method == 'cdek':
            if not delivery_detail:
                raise forms.ValidationError(
                    self.error_messages['delivery_detail_missed'],
                    code='delivery_detail_missed'
                )
            else:
                if delivery_detail not in self.delivery_detail_options:
                    raise forms.ValidationError(
                        self.error_messages['incorrect_delivery_detail_option'],
                        code='incorrect_delivery_detail_option'
                    )
        if not delivery_address:
            raise forms.ValidationError(
                self.error_messages['delivery_address_missed'],
                code='delivery_address_missed'
            )
        if not payment_method:
            raise forms.ValidationError(
                self.error_messages['payment_method_missed'],
                code='payment_method_missed'
            )
        elif payment_method not in self.payment_methods:
                raise forms.ValidationError(
                    self.error_messages['incorrect_payment_method'],
                    code='incorrect_payment_method'
                )
    
    def create_order(self):
        user = self.user
        cart = Cart(self.request)

        name = self.cleaned_data.get('name')
        email = self.cleaned_data.get('email')
        phone_number = self.cleaned_data.get('phone_number')
        
        delivery_method = self.cleaned_data.get('delivery_method')
        delivery_detail = self.cleaned_data.get('delivery_detail')
        delivery_address = self.cleaned_data.get('delivery_address')
        delivery_tariff = self.cleaned_data.get('delivery_tariff')
        delivery_comment = self.cleaned_data.get('delivery_comment')

        payment_method = self.cleaned_data.get('payment_method')


        # Check if some order has already exists in session
        order = self.request.order

        # Create new order if session does not have any
        if not order:
            order = Order.objects.create(
                user=user,
                name=name,
                email=email,
                phone_number=phone_number,

                delivery_method=delivery_method,
                delivery_detail=delivery_detail,
                delivery_address=delivery_address,
                delivery_tariff=delivery_tariff,
                delivery_comment=delivery_comment,

                payment_method=payment_method,
                
            )

            for item in cart:
                OrderItem.objects.create(
                    order=order,
                    product=item['product'],
                    price=item['price'],
                    quantity=item['quantity']
                )

                item['product'].number_in_stock -= item['quantity']
                item['product'].number_reserved += item['quantity']
                item['product'].save()
        else:
            # Update the existsting order, if session have one

            order.user = user
            order.name = name
            order.email = email
            order.phone_number = phone_number

            order.delivery_method = delivery_method
            order.delivery_detail = delivery_detail
            order.delivery_address = delivery_address
            order.delivery_tariff = delivery_tariff
            order.delivery_comment = delivery_comment

            order.payment_method = payment_method
        
            for order_item in order.items.all():
                product = order_item.product

                product.number_in_stock += order_item.quantity
                product.number_reserved -= order_item.quantity
                product.save()

            order.items.all().delete()
            order.save()
            
            for item in cart:
                OrderItem.objects.create(
                    order=order,
                    product=item['product'],
                    price=item['price'],
                    quantity=item['quantity']
                )

                item['product'].number_in_stock -= item['quantity']
                item['product'].number_reserved += item['quantity']
                item['product'].save()

        return order

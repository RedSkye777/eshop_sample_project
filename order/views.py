from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponseBadRequest
from django.conf import settings
from django.urls import reverse
from django.contrib.admin.views.decorators import staff_member_required

from order.forms import OrderCreationForm
from order.models import Order
from main.utils import ajax_required, get_form_errors

@ajax_required
def new_order(request):

    if not request.cart:
        return redirect(reverse('cart:cart_detail'))

    if request.method == 'POST':   
        form = OrderCreationForm(data=request.POST, request=request)   

        if form.is_valid(): 
            order = form.create_order()
            # Сохраняем order_id в сессию
            request.session[settings.ORDER_SESSION_ID] = order.id
            request.session.modified = True
            # Перенаправление на страницу оплаты.
            return JsonResponse({'status': 'ok', 'redirect_to': reverse('payment:process')})
        else:
            error_list = get_form_errors(form=form)
            return JsonResponse({'status': 'ko', 'errors': error_list})

    if request.method == 'GET':
        initial_data = {}

        if request.order:
            initial_data['name'] = request.order.name
            initial_data['phone_number'] = request.order.phone_number
            initial_data['email'] = request.order.email
        else:
            if request.user.is_authenticated:
                initial_data['name'] = request.user.get_full_name()
                initial_data['phone_number'] = request.user.phone_number
                initial_data['email'] = request.user.email

        form = OrderCreationForm(request=request, initial=initial_data)

        return render(request, 'order/new_order.html', {'form': form}) 
    
    

@staff_member_required
def admin_order_detail(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    return render(request, 
                  'admin/order/detail.html',
                  {'order': order})

def order_done(request, order_id):
    """Page that shows successfully created order"""
    order = get_object_or_404(Order, id=order_id, payment_status__in=['succeeded', 'waiting_for_capture'])
    return render(request, 'order/done.html', {'order': order})

def order_canceled(request, order_id):
    """Page that shows that order has been canceled"""
    return render(request, 'order/canceled.html') 
import logging

from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.conf import settings

logger = logging.getLogger(__name__)
logger.setLevel('DEBUG')

formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler('logs/signals.log')
file_handler.setLevel('DEBUG')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

@receiver(user_logged_in)
def resave_user_cart(sender, request, user, **kwargs):
    logger.debug(sender)
    logger.debug(request)
    logger.debug(user)

    user_cart = request.session.get(settings.CART_SESSION_ID, None)
    if user_cart:
        logger.debug(user_cart)

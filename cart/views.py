from django.shortcuts import render
from django.http import JsonResponse

from main.utils import ajax_required, get_form_errors
from cart.cart import Cart
from cart import forms

@ajax_required
def cart_add_or_update(request):
    """Adding product to cart by one or updating quantity to certain number"""
    form = forms.CartAddProductForm(data=request.POST, request=request)
    
    if form.is_valid(): 
        form.add_product_to_cart()
        return JsonResponse({'status': 'ok'}, safe=False)
    else:  
        error_list = get_form_errors(form)
        return JsonResponse({'status': 'ko', 'errors': error_list}, safe=False)

@ajax_required
def cart_remove(request):  
    """Removing product from user cart"""
    form = forms.CartRemoveProductForm(data=request.POST, request=request)
    
    if form.is_valid(): 
        form.remove_product_from_cart()
        return JsonResponse({'status': 'ok'}, safe=False)
    else:  
        error = form.non_field_errors()[0]
        return JsonResponse({'status': 'ko', 'error': error}, safe=False)

def cart_detail(request):    
    cart = Cart(request)    
    return render(request, 'cart/cart.html', {'cart': cart}) 
    
from django.test import TestCase
from django.urls import reverse
from django.conf import settings

from main import models
from cart.cart import Cart

class TestCartViews(TestCase):

    def test_cart_add_or_update_view(self):

        # test ajax_required decorator works
        response = self.client.post(reverse('cart:add_to_cart'))

        self.assertEqual(
            response.status_code,
            400
        )

        # Проверяем, что view отправляет JsonResponse с инфомарцией об ошибке, если form.is_valid() == False.

        ajax_data = {'product_id': '14hdhdhf1255', 'quantity': '322'}
        response = self.client.post(reverse('cart:add_to_cart'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ko'
        )

        error_list = response.json()['errors']

        self.assertTrue(
            '"product_id" is not found or incorrect.' in error_list
        )

        # Проверяем успешное добавление товара в корзину

        product = models.Product.objects.create(
            name='Тапочки',
            price=124,
            number_in_stock=20
        )

        ajax_data = {'product_id': product.id, 'quantity': '9'}
        response = self.client.post(reverse('cart:add_to_cart'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        cart = Cart(response.wsgi_request)

        self.assertEqual(
            response.json()['status'],
            'ok'
        )
        self.assertEqual(
            len(cart),
            9
        )

        # Проверяем успешное добавление другого товара в ту же корзину с пометкой update = True

        product = models.Product.objects.create(
            name='Мяч',
            price=24,
            number_in_stock=321
        )

        ajax_data = {'product_id': product.id, 'quantity': '321', 'update': 'True'}
        response = self.client.post(reverse('cart:add_to_cart'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        cart = Cart(response.wsgi_request)

        self.assertEqual(
            response.json()['status'],
            'ok'
        )

        self.assertEqual(
            len(cart),
            330
        )

    def test_remove_from_cart_view(self):

        # test ajax_required decorator works
        response = self.client.post(reverse('cart:remove_from_cart'))

        self.assertEqual(
            response.status_code,
            400
        )

        # Проверяем, что view отправляет JsonResponse с инфомарцией об ошибке, если form.is_valid() == False.
        ajax_data = {'product_id': '14hdhdhf1255'}
        response = self.client.post(reverse('cart:remove_from_cart'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ko'
        )

        self.assertEqual(
            response.json()['error'],
            '"product_id" is not found or incorrect.'
        )

        # Проверяем успешное удаление товара из корзины

        product = models.Product.objects.create(
            name='Мяч',
            price=24,
            number_in_stock=321
        )

        ajax_data = {
            'product_id': product.id,
            'quantity': 132,
        }
    
        response = self.client.post(reverse('cart:add_to_cart'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ok'
        )

        cart = response.wsgi_request.session.get(settings.CART_SESSION_ID)

        self.assertTrue(
            str(product.id) in cart
        )

        response = self.client.post(reverse('cart:remove_from_cart'), data=ajax_data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=True)

        self.assertEqual(
            response.json()['status'],
            'ok'
        )

        cart = response.wsgi_request.session.get(settings.CART_SESSION_ID)

        self.assertFalse(
            str(product.id) in cart
        )
    
    def test_cart_detail_view(self):
        response = self.client.get(reverse('cart:cart_detail'))

        self.assertEqual(
            response.status_code,
            200
        )

        self.assertTemplateUsed(
            response,
            'cart/cart.html'
        )


        

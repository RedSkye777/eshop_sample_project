from django.test import TestCase
from django.urls import reverse
from django.conf import settings

from cart import forms
from main import models

class TestCartForms(TestCase):

    def test_cart_add_product_form(self):

        error_cases = {
            'product_id_missed': {
                'quantity': '2'
            },
            'product_does_not_exist': {
                'product_id': '0206e5af-1614-497b-9929-2c3ec49cbad7',
                'quantity': '5'
            },
            'quantity_missed': {
                'product_id': '0206e5af-1614-497b-9929-2c3ec49cbad7',
            },
            'quantity_less_than_1': {
                'product_id': '0206e5af-1614-497b-9929-2c3ec49cbad7',
                'quantity': '-1'
            },
        }

        # test request for CartAddProductForm
        response = self.client.get(reverse('home'))
        self.assertEqual(
            response.status_code,
            200
        )

        # Testing validation of main possible errors
        for error_code in error_cases:
            form = forms.CartAddProductForm(data=error_cases[error_code], request=response.wsgi_request)

            self.assertFalse(form.is_valid())
            self.assertEqual(
                len(form.errors),
                1
            )
            self.assertEqual(
                form.non_field_errors().as_data()[0].code,
                error_code
            )
        
        # Тестируем успешное первое добавление товара в ещё не инициализированную корзину

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Coke',
            price=12,
            number_in_stock=20
        )

        form_data = {
            'product_id': product.id,
            'quantity': 15,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        # Тестируем неуспешное первое добавление товара в ещё не инициализированную корзину

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Muffin',
            price=10,
            number_in_stock=15
        )

        form_data = {
            'product_id': product.id,
            'quantity': 26,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertFalse(
            form.is_valid()
        )

        self.assertEqual(
            form.non_field_errors()[0],
            'Max available product quantity is 15'
        )

        # Тестируем успешное первое добавление товара в уже инициализированную корзину

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        cart = request.session[settings.CART_SESSION_ID] = {}

        product = models.Product.objects.create(
            name='Chocolate',
            price=10,
            number_in_stock=10
        )

        form_data = {
            'product_id': product.id,
            'quantity': 10,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        # Тестируем неуспешное первое добавление товара в уже инициализированную корзину

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        cart = request.session[settings.CART_SESSION_ID] = {}

        product = models.Product.objects.create(
            name='Orange',
            price=10,
            number_in_stock=200
        )

        form_data = {
            'product_id': product.id,
            'quantity': 201,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertFalse(
            form.is_valid()
        )

        self.assertEqual(
            form.non_field_errors()[0],
            'Max available product quantity is 200'
        )

        # Тестируем обычное повторное добавление товара в корзину без update

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Coffee',
            price=10,
            number_in_stock=300
        )

        cart = request.session[settings.CART_SESSION_ID] = {str(product.id): {'quantity': 150}}

        self.assertEqual(
            cart[str(product.id)]['quantity'],
            150
        )

        form_data = {
            'product_id': product.id,
            'quantity': 149,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        # Тестируем неудачное обычное повторное добавление товара в корзину без update

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Milk',
            price=10,
            number_in_stock=100
        )

        cart = request.session[settings.CART_SESSION_ID] = {str(product.id): {'quantity': 60}}
        self.assertEqual(
            cart[str(product.id)]['quantity'],
            60
        )

        form_data = {
            'product_id': product.id,
            'quantity': 50,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertFalse(
            form.is_valid()
        )

        self.assertEqual(
            form.non_field_errors()[0],
            'Max available product quantity is 100'
        )

        # Тестируем обычное обновление колличества товара в корзине в большую сторону. Т.е update = True

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Milk shake',
            price=140,
            number_in_stock=500
        )

        cart = request.session[settings.CART_SESSION_ID] = {str(product.id): {'quantity': 340}}

        self.assertEqual(
            cart[str(product.id)]['quantity'],
            340
        )

        form_data = {
            'product_id': product.id,
            'quantity': 400,
            'update': 'True'
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        # Тестируем обычное обновление колличества товара в корзине в меньшую сторону. Т.е update = True

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Vodka',
            price=140,
            number_in_stock=500
        )

        cart = request.session[settings.CART_SESSION_ID] = {str(product.id): {'quantity': 340}}

        self.assertEqual(
            cart[str(product.id)]['quantity'],
            340
        )

        form_data = {
            'product_id': product.id,
            'quantity': 2,
            'update': 'True'
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        # Тестируем неудачное обновление колличества товара.

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Table',
            price=1400,
            number_in_stock=300
        )

        cart = request.session[settings.CART_SESSION_ID] = {str(product.id): {'quantity': 200}}

        self.assertEqual(
            cart[str(product.id)]['quantity'],
            200
        )

        form_data = {
            'product_id': product.id,
            'quantity': 301,
            'update': 'True'
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertFalse(
            form.is_valid()
        )

        self.assertEqual(
            form.non_field_errors()[0],
            'Max available product quantity is 300'
        )

        # Тестируем add_product_to_cart method with update = False

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Chair',
            price=135,
            number_in_stock=302
        )

        form_data = {
            'product_id': product.id,
            'quantity': 102,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        form.add_product_to_cart()

        cart = request.session[settings.CART_SESSION_ID]

        self.assertTrue(
            str(product.id) in cart
        )

        self.assertEqual(
            cart[str(product.id)]['quantity'],
            102
        )

        # Тестируем add_product_to_cart method with update = True

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Ceiling',
            price=135,
            number_in_stock=1000
        )

        form_data = {
            'product_id': product.id,
            'quantity': 600,
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        form.add_product_to_cart()

        cart = request.session[settings.CART_SESSION_ID]

        self.assertTrue(
            str(product.id) in cart
        )

        self.assertEqual(
            cart[str(product.id)]['quantity'],
            600
        )

        form_data = {
            'product_id': product.id,
            'quantity': 1000,
            'update': 'True'
        }

        form = forms.CartAddProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        form.add_product_to_cart()

        cart = request.session[settings.CART_SESSION_ID]

        self.assertTrue(
            str(product.id) in cart
        )

        self.assertEqual(
            cart[str(product.id)]['quantity'],
            1000
        )
    
    def test_cart_remove_product_form(self):

        error_cases = {
            'product_id_missed': {
                '': ''
            },
            'product_does_not_exist': {
                'product_id': '0206e5af-1614-497b-9929-2c3ec49cbad7',
            },
        }

        # test request for CartAddProductForm
        response = self.client.get(reverse('home'))
        self.assertEqual(
            response.status_code,
            200
        )

        # Testing validation of main possible errors
        for error_code in error_cases:
            form = forms.CartRemoveProductForm(data=error_cases[error_code], request=response.wsgi_request)

            self.assertFalse(form.is_valid())
            self.assertEqual(
                len(form.errors),
                1
            )
            self.assertEqual(
                form.non_field_errors().as_data()[0].code,
                error_code
            )
        
        # Test cart_is_not_determined error

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Pillow',
            price=12,
            number_in_stock=324
        )

        form_data = {
            'product_id': product.id,
        }

        form = forms.CartRemoveProductForm(data=form_data, request=request)

        self.assertFalse(
            form.is_valid()
        )

        self.assertEqual(
            form.non_field_errors()[0],
            'Cart is not determined in the user session.'
        )

        # Test product_is_not_in_cart error

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        request.session[settings.CART_SESSION_ID] = {'test_id_product': {'quantity': '23'}}

        product = models.Product.objects.create(
            name='Bag',
            price=122,
            number_in_stock=3324
        )

        form_data = {
            'product_id': product.id,
        }

        form = forms.CartRemoveProductForm(data=form_data, request=request)

        self.assertFalse(
            form.is_valid()
        )

        self.assertEqual(
            form.non_field_errors()[0],
            'Product does not belong to the user cart.'
        )

        # Тестируем remove_product_from_cart method

            # Вспомогательный request object
        request = self.client.get(reverse('home')).wsgi_request

        product = models.Product.objects.create(
            name='Chair',
            price=135,
            number_in_stock=302
        )

        cart = request.session[settings.CART_SESSION_ID] = {str(product.id): {'quantity': '32'}}

        self.assertTrue(
            str(product.id) in cart
        )

        form_data = {
            'product_id': product.id,
        }

        form = forms.CartRemoveProductForm(data=form_data, request=request)

        self.assertTrue(
            form.is_valid()
        )

        form.remove_product_from_cart()

        self.assertFalse(
            str(product.id) in cart
        )



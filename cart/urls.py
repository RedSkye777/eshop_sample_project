from django.urls import path

from cart import views

app_name = 'cart'

urlpatterns = [
    path(
        'add/',
        views.cart_add_or_update,
        name='add_to_cart'
    ),
    path(
        'remove/',
        views.cart_remove,
        name='remove_from_cart'
    ),
    path(
        '',
        views.cart_detail,
        name='cart_detail'
    ),
]

from django import forms
from django.conf import settings

from main import models
from cart.cart import Cart

class CartAddProductForm(forms.Form):
    product_id= forms.UUIDField(required=False)
    quantity = forms.IntegerField(required=False)
    update = forms.BooleanField(required=False, initial=False)

    error_messages = {
        'product_id_missed': '"product_id" is not found or incorrect.',
        'product_does_not_exist': 'Product does not exist.',
        'quantity_missed': '"quantity" is not found.',
        'quantity_less_than_1': 'Quantity must not be less than 1.',
        'quantity_more_than_number_in_stock': 'Quantity must be less than product number in stock.'
    }

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    def clean(self):
        cart = self.request.session.get(settings.CART_SESSION_ID)

        product_id = self.cleaned_data.get('product_id')
        quantity = self.cleaned_data.get('quantity')
        update = self.cleaned_data.get('update')

        if not product_id:
            raise forms.ValidationError(
                self.error_messages['product_id_missed'],
                code='product_id_missed'
            )
        if not quantity:
            raise forms.ValidationError(
                self.error_messages['quantity_missed'],
                code='quantity_missed'
            )
        elif quantity < 1:
            raise forms.ValidationError(
                self.error_messages['quantity_less_than_1'],
                code='quantity_less_than_1'
            )
        if product_id:
            if not models.Product.objects.active().filter(id=product_id).exists():
                raise forms.ValidationError(
                    self.error_messages['product_does_not_exist'],
                    code='product_does_not_exist'
                )
            else:
                product = models.Product.objects.get(id=product_id)
        if cart:
            if str(product_id) in cart:
                product_quantity_in_cart = cart[str(product_id)]['quantity']
                if not update:
                    # Обычное повторное добавление товара в корзину без update quantity
                    if (product_quantity_in_cart + quantity) > product.number_in_stock:
                        raise forms.ValidationError(
                            'Max available product quantity is {}'.format(product.number_in_stock),
                            code='quantity_more_than_number_in_stock'
                        )
                else:
                    # Обычное обновление колличества товара в корзине. Т.е update = True
                    if quantity > product.number_in_stock:
                        raise forms.ValidationError(
                            'Max available product quantity is {}'.format(product.number_in_stock),
                            code='quantity_more_than_number_in_stock'
                        )
            # Абсолютно первое добавление товара в уже инициализированную корзину
            elif quantity > product.number_in_stock:
                raise forms.ValidationError(
                    'Max available product quantity is {}'.format(product.number_in_stock),
                    code='quantity_more_than_number_in_stock'
                )
        # Абсолютно первое добавление товара в ещё не инициализированную корзину
        elif quantity > product.number_in_stock:
            raise forms.ValidationError(
                'Max available product quantity is {}'.format(product.number_in_stock),
                code='quantity_more_than_number_in_stock'
            )

    def add_product_to_cart(self):
        product_id = self.cleaned_data.get('product_id')
        quantity = self.cleaned_data.get('quantity')
        update = self.cleaned_data.get('update')

        cart = Cart(self.request)
        product = models.Product.objects.get(id=product_id)

        cart.add(product=product,                 
            quantity=quantity,                 
            update_quantity=update
        )

class CartRemoveProductForm(forms.Form):
    product_id = forms.UUIDField(required=False)

    error_messages = {
        'product_id_missed': '"product_id" is not found or incorrect.',
        'product_does_not_exist': 'Product does not exist.',
        'cart_is_not_determined': 'Cart is not determined in the user session.',
        'product_is_not_in_cart': 'Product does not belong to the user cart.'
        
    }

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    def clean(self):
        cart = self.request.session.get(settings.CART_SESSION_ID)

        product_id = self.cleaned_data.get('product_id')

        if not product_id:
            raise forms.ValidationError(
                self.error_messages['product_id_missed'],
                code='product_id_missed'
            )
        elif not models.Product.objects.active().filter(id=product_id).exists():
                raise forms.ValidationError(
                    self.error_messages['product_does_not_exist'],
                    code='product_does_not_exist'
                )
        if not cart:
            raise forms.ValidationError(
                self.error_messages['cart_is_not_determined'],
                code='cart_is_not_determined'
            )
        elif str(product_id) not in cart:
                raise forms.ValidationError(
                    self.error_messages['product_is_not_in_cart'],
                    code='product_is_not_in_cart'
                )
    
    def remove_product_from_cart(self):
        product_id = self.cleaned_data.get('product_id')

        cart = Cart(self.request)    
        product = models.Product.objects.get(id=product_id)

        cart.remove(product)    
    
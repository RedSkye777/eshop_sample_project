from django.conf import settings

from cart.cart import Cart

def cart_middleware(get_response):
    def middleware(request):
        if not settings.CART_SESSION_ID in request.session:
            request.cart = None
        else:
            cart = request.session[settings.CART_SESSION_ID]
            if not cart:
                request.cart = None
            else:
                request.cart = Cart(request)
        response = get_response(request)
        return response
    return middleware
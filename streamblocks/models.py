from django.db import models

from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill

from ckeditor.fields import RichTextField

from main.models import Product

class RichTextBlock(models.Model):
    block_template = "streamblocks/rich_text_block.html"
    text = RichTextField(blank=True, null=True)   

    class Meta:
        # This will use as name of block in admin
        verbose_name="Блок с текстом"
        verbose_name_plural="Блок с текстом"

# list of objects
class ImageBlock(models.Model):
    block_template = "streamblocks/image_block.html"
    image = ProcessedImageField(
        upload_to='page_content/',
        format='JPEG',
        options={'quality': 50},
        blank=True,
        null=True,
        verbose_name='Изображение',
    )

    # StreamField option for list of objects
    as_list = True

    class Meta:
        verbose_name="Блок с изображениями"
        verbose_name_plural="Блок с изображениями"
        

class ProductBlock(models.Model):
    block_template = "streamblocks/product_block.html"
    # title = models.CharField(max_length=50, verbose_name='Заголовок блока')
    product = models.ForeignKey(
        Product, 
        verbose_name=('Товар'), 
        on_delete=models.CASCADE,
        null=True
    )

    # StreamField option for list of objects
    as_list = True

    class Meta:
        # This will use as name of block in admin
        verbose_name="Блок с товаром"
        verbose_name_plural="Блок с товарами"

class SliderBlock(models.Model):
    block_template = "streamblocks/slider_block.html"

    image = ProcessedImageField(
        upload_to='page_content/',
        format='JPEG',
        options={'quality': 50},
        blank=True,
        null=True,
        verbose_name='Изображение',
    )

    # StreamField option for list of objects
    as_list = True

    class Meta:
        # This will use as name of block in admin
        verbose_name="Слайдер"
        verbose_name_plural="Слайдеры"


# Register blocks for StreamField as list of models
STREAMBLOCKS_MODELS = [
    RichTextBlock,
    ImageBlock,
    ProductBlock,
    SliderBlock
]
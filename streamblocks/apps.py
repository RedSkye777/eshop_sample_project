from django.apps import AppConfig


class StreamblocksConfig(AppConfig):
    name = 'streamblocks'

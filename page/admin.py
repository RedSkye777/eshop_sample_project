from django.contrib import admin

from page.models import HomePage, AboutUsPage, ContactUsPage, PaymentPage, RepliesPage, ShippingPage

@admin.register(AboutUsPage)
class AboutUsPageAdmin(admin.ModelAdmin):
    list_display = ['name', 'title']
    fieldsets = (
        ('Содержимое страницы', {
            'fields': ('name', 'stream'),
        }),
        ('SEO', {
            'fields': ('title', 'description', 'keywords')
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(HomePage)
class HomePageAdmin(admin.ModelAdmin):
    list_display = ['name', 'title']
    fieldsets = (
        ('Содержимое страницы', {
            'fields': ('name', 'stream'),
        }),
        ('SEO', {
            'fields': ('title', 'description', 'keywords')
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(ContactUsPage)
class ContactUsPageAdmin(admin.ModelAdmin):
    list_display = ['name', 'title']
    fieldsets = (
        ('Содержимое страницы', {
            'fields': ('name', 'stream'),
        }),
        ('SEO', {
            'fields': ('title', 'description', 'keywords')
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(PaymentPage)
class PaymentPageAdmin(admin.ModelAdmin):
    list_display = ['name', 'title']
    fieldsets = (
        ('Содержимое страницы', {
            'fields': ('name', 'stream'),
        }),
        ('SEO', {
            'fields': ('title', 'description', 'keywords')
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(RepliesPage)
class RepliesPageAdmin(admin.ModelAdmin):
    list_display = ['name', 'title']
    fieldsets = (
        ('Содержимое страницы', {
            'fields': ('name', 'stream'),
        }),
        ('SEO', {
            'fields': ('title', 'description', 'keywords')
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(ShippingPage)
class ShippingPageAdmin(admin.ModelAdmin):
    list_display = ['name', 'title']
    fieldsets = (
        ('Содержимое страницы', {
            'fields': ('name', 'stream'),
        }),
        ('SEO', {
            'fields': ('title', 'description', 'keywords')
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
from django.apps import AppConfig


class PageConfig(AppConfig):
    name = 'page'
    
    verbose_name = 'Страницы'
    verbose_name_plural = 'Страницы'
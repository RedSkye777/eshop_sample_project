from django.db import models
from django.core.exceptions import ValidationError

from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill

from main.models import Product

from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

from streamfield.fields import StreamField
from streamblocks.models import RichTextBlock, ImageBlock, ProductBlock, SliderBlock

class Page(models.Model):
    name = models.CharField(
        max_length=100, 
        verbose_name='Название страницы', 
        help_text='Отображается в Breadcrumb'
    )
    title =  models.CharField(
        max_length=75, 
        verbose_name='Мета-тэг Title'
    )
    description = models.CharField(
        max_length=200, 
        verbose_name='Мета-тэг Description', 
        blank=True
    )
    keywords = models.CharField(
        max_length=200, 
        verbose_name='Мета-тэг Keywords', 
        blank=True
    )
    
    class Meta:
        abstract = True
    
    def validate_max_count(self):
        qs = self.__class__.objects.all()

        if self.pk:
            qs = qs.exclude(pk=self.pk)

        if qs.count() >= self.max_count:
            raise ValidationError('Превышено допустимое колличество копий данной страницы')

    def save(self, *args, **kwargs):
        self.validate_max_count()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
    
class AboutUsPage(Page):
    max_count = models.PositiveIntegerField(verbose_name='Максимальное колличество копий данной страницы', default=1)
    stream = StreamField(
        model_list=[ 
            RichTextBlock,
            ImageBlock
        ],
        verbose_name="Содержимое страницы"
    )

    class Meta:
        verbose_name = 'Информация о компании'
        verbose_name_plural = 'Информация о компании'


class HomePage(Page):
    max_count = models.PositiveIntegerField(verbose_name='Максимальное колличество копий данной страницы', default=1)
    stream = StreamField(
        model_list=[ 
            RichTextBlock,
            ImageBlock,
            ProductBlock,
            SliderBlock,
        ],
        verbose_name="Содержимое страницы"
    )

    class Meta:
        verbose_name = 'Главная страница'
        verbose_name_plural = 'Главная страница'

class ShippingPage(Page):
    max_count = models.PositiveIntegerField(verbose_name='Максимальное колличество копий данной страницы', default=1)
    stream = StreamField(
        model_list=[ 
            RichTextBlock,
            ImageBlock,
            ProductBlock,
            SliderBlock,
        ],
        verbose_name="Содержимое страницы"
    )

    class Meta:
        verbose_name = 'Информация о доставке'
        verbose_name_plural = 'Информация о доставке'

class PaymentPage(Page):
    max_count = models.PositiveIntegerField(verbose_name='Максимальное колличество копий данной страницы', default=1)
    stream = StreamField(
        model_list=[ 
            RichTextBlock,
            ImageBlock,
            ProductBlock,
            SliderBlock,
        ],
        verbose_name="Содержимое страницы"
    )

    class Meta:
        verbose_name = 'Информация об оплате'
        verbose_name_plural = 'Информация об оплате'

class ContactUsPage(Page):
    max_count = models.PositiveIntegerField(verbose_name='Максимальное колличество копий данной страницы', default=1)
    stream = StreamField(
        model_list=[ 
            RichTextBlock,
            ImageBlock,
            ProductBlock,
            SliderBlock,
        ],
        verbose_name="Содержимое страницы"
    )

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'

class RepliesPage(Page):
    max_count = models.PositiveIntegerField(verbose_name='Максимальное колличество копий данной страницы', default=1)
    stream = StreamField(
        model_list=[ 
            RichTextBlock,
            ImageBlock,
            ProductBlock,
            SliderBlock,
        ],
        verbose_name="Содержимое страницы"
    )

    class Meta:
        verbose_name = 'Отзывы'
        verbose_name_plural = 'Отзывы'




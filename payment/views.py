import uuid

from yandex_checkout import Payment, Refund

from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import ObjectDoesNotExist
from django.urls import reverse
from django.conf import settings

from order.models import Order
from payment.tasks import check_order_status
from cart.cart import Cart
from page.models import PaymentPage

def payment_detail(request):
    
    qs = PaymentPage.objects.all()

    if qs.exists():
        page = qs.first()
    else:
        page = PaymentPage.objects.create(name='Информация об оплате', title='Информация об оплате')

    context = {
        'page': page
    }
    context = {
        'page': 'page'
    }

    return render(request, 'payment/detail.html', context)

def payment_process(request):

    if not request.order:
        return redirect(reverse('order:new_order'))
    
    order = request.order
    payment = Payment.create({
        "amount": {
            "value": str(order.get_total_cost()),
            "currency": "RUB"
        },
        "confirmation": {
            "type": "redirect",
            "return_url": request.build_absolute_uri(reverse('payment:check_payment_status'))
        },
        "capture": True,
        "description": "Заказ №1"
    }, uuid.uuid4())

    order.payment_id = payment.id
    order.save()
    
    # check_order_status.apply_async((order.payment_id, order.id))
    return redirect(payment.confirmation.confirmation_url)

def check_payment_status(request):

    if not request.order:
        return redirect(reverse('order:new_order'))

    cart = Cart(request)
    order = request.order
    payment_id = order.payment_id

    payment = Payment.find_one(payment_id)

    if payment.status == 'succeeded':
        order.payment_status = 'succeeded'

        for orderLine in order.items.all():
            product = orderLine.product
            quantity = orderLine.quantity

            product.number_reserved -= quantity
            product.number_purchased += quantity
            product.save()
        
        order.save()
        cart.clear()
        del request.session['order']

        return redirect(reverse('order:done', kwargs={'order_id': order.id}))
    
    elif payment.status == 'canceled':
        order.payment_status = 'canceled'

        for orderLine in order.items.all():
            product = orderLine.product
            quantity = orderLine.quantity

            product.number_reserved -= quantity
            product.number_in_stock += quantity
            product.save()
        
        order.save()

        return redirect(reverse('order:canceled', kwargs={'order_id': order.id}))
    
    elif payment.status == 'waiting_for_capture':
        order.payment_status = 'waiting_for_capture'

        for orderLine in order.items.all():
            product = orderLine.product
            quantity = orderLine.quantity

            product.number_reserved -= quantity
            product.number_purchased += quantity
            product.save()
        
        order.save()
        cart.clear()
        del request.session['order']

        return redirect(reverse('order:done', kwargs={'order_id': order.id}))
    
    elif payment.status == 'pending':
       return redirect(reverse('order:new_order'))

    else:
        return redirect(reverse('order:canceled', kwargs={'order_id': order.id}))

def payment_refund(order_id, amount):
    response = {}
    error_list = []

    try:
        order = Order.objects.get(id=order_id)
        payment_id = order.payment_id

        payment = Payment.find_one(payment_id)

        if payment.status == 'succeeded':
            refund = Refund.create({
                "amount": {
                    "value": amount,
                    "currency": "RUB"
                },
                "payment_id": payment_id
            })

            if refund.status == 'succeeded':
                return 'Ok'
            else:
                error_list.append(
                    'Refund was not successful.'
                )
    except ObjectDoesNotExist:
        error_list.append(
            'Order does not exist.'
        )
    except AttributeError:
        error_list.append(
            'Order does not have payment_id attribute.'
        )
    return error_list



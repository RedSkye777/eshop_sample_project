import logging

from yandex_checkout import Payment
from datetime import datetime, timezone


from django.db.models import ObjectDoesNotExist
from django.conf import settings 

from config.celery_app import app
from order.models import Order
from main.models import Product

logger = logging.getLogger(__name__)
logger.setLevel('INFO')

formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler('logs/tasks.log')
file_handler.setLevel('INFO')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

@app.task
def check_order_status(payment_id, order_id):
    payment = Payment.find_one(payment_id)

    try:
        order = Order.objects.get(id=order_id)
    except ObjectDoesNotExist:
        logger.error('Error during checking order status. Order has not been found. Order id - {}'.format(order_id))
        return False

    if payment.status == 'succeeded':
        order.status = 'succeeded'

        for orderLine in order.items.all():
            product = orderLine.product
            quantity = orderLine.quantity

            product.number_reserved -= quantity
            product.number_purchased += quantity
            product.save()
        
        order.save()

    elif payment.status == 'canceled':
        order.status = 'canceled'

        for orderLine in order.items.all():
            product = orderLine.product
            quantity = orderLine.quantity

            product.number_reserved -= quantity
            product.number_in_stock += quantity
            product.save()
        
        order.save()
    
    elif payment.status == 'waiting_for_capture':
        order.status = 'waiting_for_capture'

        for orderLine in order.items.all():
            product = orderLine.product
            quantity = orderLine.quantity

            product.number_reserved -= quantity
            product.number_purchased += quantity
            product.save()
        
        order.save()
    
    elif payment.status == 'pending':

        # Check if time for payment has been expired
        hour_since_creation = (datetime.now(timezone.utc) - order.created).seconds / 60 / 60

        if hour_since_creation > settings.ORDER_EXPIRES_IN:
            order.status = 'canceled'

            for orderLine in order.items.all():
                product = orderLine.product
                quantity = orderLine.quantity

                product.number_reserved -= quantity
                product.number_in_stock += quantity
                product.save()
            
            order.save()
            logger.info('Payment time has expired. Order id - {}'.format(order.id))

        else:
            logger.info('Restarting task to check order status. Order id - {}'.format(order.id))
            check_order_status.apply_async((payment_id, order_id), countdown=30)

    else:
        logger.error('Order status has not been determined. Order id - {}'.format(order.id))
        
    


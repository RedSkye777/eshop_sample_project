from django.urls import path

from payment import views

app_name = 'payment'

urlpatterns = [
    path('check-payment-status/', views.check_payment_status, name='check_payment_status'),
    path('process/', views.payment_process, name='process'),
    path('', views.payment_detail, name='detail')
]

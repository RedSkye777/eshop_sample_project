from django.test import TestCase
from django.urls import reverse
from django.conf import settings

from payment import views
from order.models import Order, OrderItem
from main.models import Product

class TestPaymentViews(TestCase):

    def test_payment_canceled(self):
        response = self.client.get(reverse('payment:canceled'))

        self.assertTrue(
            response.status_code,
            200
        )

        self.assertTemplateUsed(
            response,
            'payment/canceled.html'
        )
    
    def test_payment_done(self):
        response = self.client.get(reverse('payment:done'))

        self.assertTrue(
            response.status_code,
            200
        )

        self.assertTemplateUsed(
            response,
            'payment/done.html'
        )
    
    def test_payment_process_without_order(self):

        # Trying to access payment process page

        response = self.client.get(reverse('payment:process'))

        self.assertTrue(
            response.status_code,
            200
        )

        self.assertTemplateUsed(
            response,
            'payment/payment_without_order.html'
        )

    
    def test_payment_process_get_method(self):

        # Creating order and saving its id to the session

        order = Order.objects.create(
            name='name',
            email='email@gmail.com',
            phone_number='123321',
            delivery_method='delivery',
            postal_code='123321',
            address='Moscow',
            payment_method='online',
        )

        session = self.client.session
        session[settings.ORDER_SESSION_ID] = order.id
        session.save()

        # Trying to access payment process page

        response = self.client.get(reverse('payment:process'))

        self.assertTrue(
            response.status_code,
            200
        )

        self.assertTemplateUsed(
            response,
            'payment/process.html'
        )

        self.assertTrue(
            response.context['client_token']
        )
    
    def test_payment_process_post_method_works_correctly(self):

        # Creating some products for testing
        Product.objects.create(
            name='Apple',
            price=23,
            number_in_stock=200
        )

        Product.objects.create(
            name='Cabbage',
            price=12,
            number_in_stock=100
        )

        # Creating order with products, saving its id to the session

        order = Order.objects.create(
            name='name',
            email='email@gmail.com',
            phone_number='123321',
            delivery_method='delivery',
            postal_code='123321',
            address='Moscow',
            payment_method='online',
        )

        for product in Product.objects.all():
            OrderItem.objects.create(
                order=order,
                product=product,
                price=product.price,
                quantity=1
            )
        
        session = self.client.session
        session[settings.ORDER_SESSION_ID] = order.id
        session.save()

        # Trying to access payment process page

        response = self.client.post(reverse('payment:process'), {'payment_method_nonce': 'fake-valid-no-billing-address-nonce'}, follow=True)

        self.assertRedirects(
            response,
            reverse('payment:done')
        )

        order = Order.objects.get(id=order.id)

        self.assertEqual(
            order.paid,
            True
        )

        self.assertNotEqual(
            order.braintree_id,
            None
        )

        self.assertTrue(
            settings.ORDER_SESSION_ID not in self.client.session
        )
    
    def test_payment_process_post_method_works_incorrectly(self):

        # Creating order, saving its id to the session

        order = Order.objects.create(
            name='name',
            email='email@gmail.com',
            phone_number='123321',
            delivery_method='delivery',
            postal_code='123321',
            address='Moscow',
            payment_method='online',
        )

        session = self.client.session
        session[settings.ORDER_SESSION_ID] = order.id
        session.save()

        # Trying to access payment process page

        response = self.client.post(reverse('payment:process'), {'payment_method_nonce': 'invalid-nonce'}, follow=True)

        self.assertRedirects(
            response,
            reverse('payment:canceled')
        )
        
